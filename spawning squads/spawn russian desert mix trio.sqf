[] spawn {

_GL="CUP_O_RU_Soldier_GL_M_EMR_V2";


_R="CUP_O_RU_Soldier_M_EMR_V2";

_M="CUP_O_RU_Soldier_Medic_M_EMR_V2";


/////////////////////////////////

platoonGroup = createGroup [west, true];

_platoonComposition=[ 
	//Classname, rank, hierarchy in group, team
	[_GL, "SERGEANT",2, "MAIN"],			//Squad lead

	[_R, "CORPORAL",3, "MAIN"],			//
	[_M, "CORPORAL",4, "MAIN"]			//

];



//////////////////////Spawning the platoon

{
	_x params ["_unitClassname","_unitRank","_orderInGroup","_myTeamName"];
	_initString=format [
		"this setVariable ['ace_medical_damageThreshold',1000];
		this setSkill 1;
		this disableAI 'AIMINGERROR';
		this setVariable ['lambs_danger_disableAI',true];
		this joinAsSilent [platoonGroup, %1];
		[this,'%2'] spawn { 
			_this params ['_myUnit','_myTeam'];
			sleep 2;
			_myUnit assignTeam _myTeam;
			};
		doStop this;"
	, _orderInGroup, _myTeamName];

	_unitClassname createUnit [
		getpos player,
		platoonGroup,
		_initString,
		1,
		_unitRank
	];
} forEach _platoonComposition;


//////////////////////////////////////////////

player joinAsSilent [platoonGroup, 1];
platoonGroup selectLeader player;
//player setRank "LIEUTENANT";

//////////////////Custom kit

//GL

	Current_unit= (units player select 1);
	Current_unit setUnitLoadout [["CUP_arifle_AK74M_GL_1p63","","","CUP_optic_1p63",["CUP_30Rnd_545x39_AK74M_M",30],["CUP_1Rnd_HE_GP25_M",1],""],[],["hgun_Rook40_F","","","",["16Rnd_9x21_Mag",17],[],""],["CUP_U_O_RUS_Soldier_VKPO_MSV_BeigeDigital_gloves_pads",[["FirstAidKit",1],["CUP_NVG_HMNVS_Hide",1],["CUP_30Rnd_545x39_AK74M_M",5,30],["CUP_30Rnd_TE1_Green_Tracer_545x39_AK74M_M",1,30]]],["CUP_Vest_RUS_6B45_Sh117_VOG_Nut",[["CUP_30Rnd_TE1_Green_Tracer_545x39_AK74M_M",1,30],["16Rnd_9x21_Mag",2,17],["CUP_HandGrenade_RGD5",2,1],["CUP_1Rnd_HE_GP25_M",5,1],["SmokeShell",1,1],["SmokeShellRed",1,1],["Chemlight_red",2,1],["CUP_1Rnd_SMOKE_GP25_M",2,1],["CUP_1Rnd_SmokeRed_GP25_M",1,1],["CUP_IlumFlareWhite_GP25_M",2,1],["CUP_IlumFlareRed_GP25_M",1,1]]],[],"CUP_H_RUS_6B27_cover_headset_goggles","",["Binocular","","","",[],[],""],["ItemMap","","ItemRadio","ItemCompass","ItemWatch",""]];
	
	_identity=["Lev Zubov","WhiteHead_26","male03rus"];
	_name=_identity select 0;
	sleep 1;
	Current_unit setName [_name,(_name splitString " ") select 0,(_name splitString " ") select 1];
	Current_unit setFace _identity#1;
	Current_unit setSpeaker _identity#2;
	

//R
	Current_unit= (units player select 2);
	Current_unit setUnitLoadout [["CUP_arifle_AKS74","","","CUP_optic_1p63",["CUP_30Rnd_545x39_AK74M_M",30],[],""],[],[],["CUP_U_O_RUS_Soldier_VKPO_MSV_BeigeDigital_rolled_up_gloves_pads",[["FirstAidKit",1],["CUP_NVG_HMNVS_Hide",1],["CUP_30Rnd_545x39_AK74M_M",6,30]]],["CUP_Vest_RUS_6B45_Sh117_Nut",[["CUP_HandGrenade_RGD5",2,1],["SmokeShell",1,1],["SmokeShellRed",1,1],["Chemlight_red",2,1],["CUP_30Rnd_545x39_AK74M_M",5,30]]],[],"CUP_H_RUS_6B46","G_Squares",[],["ItemMap","","ItemRadio","ItemCompass","ItemWatch",""]];
	
	_identity=["Arkadiy Kuznetsov","WhiteHead_11","male01rus"];
	_name=_identity select 0;
	sleep 1;
	Current_unit setName [_name,(_name splitString " ") select 0,(_name splitString " ") select 1];
	Current_unit setFace _identity#1;
	Current_unit setSpeaker _identity#2;
	


///M

	Current_unit= (units player select 3);
	Current_unit setUnitLoadout [["CUP_arifle_AKS74U","","","",["CUP_30Rnd_545x39_AK74M_M",30],[],""],[],["hgun_Rook40_F","","","",["16Rnd_9x21_Mag",17],[],""],["CUP_U_O_RUS_Soldier_VKPO_MSV_BeigeDigital_gloves_pads",[["FirstAidKit",1],["CUP_NVG_HMNVS_Hide",1],["CUP_30Rnd_545x39_AK74M_M",6,30]]],["CUP_Vest_RUS_6B45_Sh117",[["16Rnd_9x21_Mag",2,17],["CUP_HandGrenade_RGD5",2,1],["SmokeShell",1,1],["SmokeShellRed",1,1],["SmokeShellOrange",1,1],["SmokeShellYellow",1,1],["Chemlight_red",2,1],["CUP_30Rnd_545x39_AK74M_M",1,30]]],["CUP_O_RUS_Patrol_bag_Summer",[["FirstAidKit",5],["Medikit",1],["Chemlight_blue",5,1],["ACE_Chemlight_HiBlue",5,1],["SmokeShellGreen",5,1],["SmokeShellBlue",5,1]]],"CUP_H_RUS_6B46","",["Binocular","","","",[],[],""],["ItemMap","","ItemRadio","ItemCompass","ItemWatch",""]];
	
	_identity=["Svetlana Kameneva","FaceW10","cup_d_female01_ru"];
	_name=_identity select 0;
	sleep 1;
	Current_unit setName [_name,(_name splitString " ") select 0,(_name splitString " ") select 1];
	Current_unit setFace _identity#1;
	Current_unit setSpeaker _identity#2;
	
}
////////////////////////////////////////////////


/*
[] spawn {
	sleep 5;
	{player setVariable["factionType","modern_russia_woodland"];AIDEBUG=True;_x  spawn convert;} forEach (units player - [player]);
};
*/