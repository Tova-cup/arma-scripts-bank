_SL="CUP_B_USMC_MARSOC_TL";


_PIL="CUP_B_USMC_Pilot_des";

/////////////////////////////////

platoonGroup = createGroup [west, true];

//I put all the SL first in the hierarchy for easy selection
_platoonComposition=[ 
	//Classname, rank, hierarchy in group, team
	[_SL, "SERGEANT",2, "MAIN"],			//Squad lead


	//Air Transport 
	[_PIL, "LIEUTENANT",21, "YELLOW"],			//Pilot
	[_PIL, "SERGEANT",22, "YELLOW"]			//co-Pilot

];



//////////////////////Spawning the platoon

{
	_x params ["_unitClassname","_unitRank","_orderInGroup","_myTeamName"];
	_initString=format [
		"this setVariable ['ace_medical_damageThreshold',1000];
		this setSkill 1;
		this disableAI 'AIMINGERROR';
		this setVariable ['lambs_danger_disableAI',true];
		this joinAsSilent [platoonGroup, %1];
		[this,'%2'] spawn { 
			_this params ['_myUnit','_myTeam'];
			sleep 2;
			_myUnit assignTeam _myTeam;
			};
		doStop this;"
	, _orderInGroup, _myTeamName];

	_unitClassname createUnit [
		getpos player,
		platoonGroup,
		_initString,
		1,
		_unitRank
	];
} forEach _platoonComposition;


//////////////////////////////////////////////

player joinAsSilent [platoonGroup, 1];
platoonGroup selectLeader player;
player setRank "LIEUTENANT";

//////////////////Custom kit

// player
	player setUnitLoadout [["CUP_arifle_HK416_Desert","","CUP_acc_ANPEQ_15_Flashlight_Black_L","",["CUP_30Rnd_556x45_PMAG_BLACK_RPL",30],[],""],[],["CUP_hgun_M17_Black","","","",["CUP_21Rnd_9x19_M17_Black",21],[],""],["CUP_I_B_PMC_Unit_23",[["ACE_DAGR",1],["ACE_MapTools",1],["ACE_Flashlight_XL50",1],["ACE_MRE_MeatballsPasta",1],["ACE_WaterBottle",1],["ACE_packingBandage",11],["ACE_epinephrine",2],["ACE_morphine",2],["CUP_NVG_PVS15_black",1]]],["CUP_V_PMC_CIRAS_Khaki_Patrol_Marsoc1",[["ItemGPS",1],["CUP_muzzle_snds_M16",1],["SmokeShellBlue",1,1],["SmokeShell",2,1],["CUP_HandGrenade_M67",1,1],["CUP_30Rnd_556x45_PMAG_BLACK_RPL",6,30]]],["B_RadioBag_01_eaf_F",[]],"CUP_H_PMC_EP_Headset","CUP_G_Tan_Scarf_Shades_GPSCombo",["CUP_LRTV","","","",["Laserbatteries",1],[],""],["ItemMap","","ItemRadio","ItemCompass","ItemWatch",""]];
	player setFace "FaceW19";
	player setSpeaker "CUP_D_Female01_EN";
	player setObjectTextureGlobal [1, "cup\creatures\people\military\cup_creatures_people_military_usarmy\data\bduv2\bduv2_dcu_co.paa"]; 

//SL
	(units player select 1) setUnitLoadout [["CUP_arifle_HK416_M203_Desert","","CUP_acc_ANPEQ_15_Flashlight_Black_L","",["CUP_30Rnd_556x45_PMAG_BLACK_RPL",30],[],""],[],["CUP_hgun_M17_Black","","","",["CUP_21Rnd_9x19_M17_Black",21],[],""],["CUP_I_B_PARA_Unit_10",[["ACE_DAGR",1],["ACE_MapTools",1],["ACE_Flashlight_XL50",1],["ACE_MRE_MeatballsPasta",1],["ACE_WaterBottle",1],["ACE_packingBandage",11],["ACE_epinephrine",2],["ACE_morphine",2],["CUP_NVG_PVS15_black",1]]],["CUP_V_PMC_CIRAS_Coyote_Patrol_Marsoc1",[["ItemGPS",1],["CUP_muzzle_snds_M16",1],["SmokeShellBlue",1,1],["SmokeShell",2,1],["CUP_HandGrenade_M67",1,1],["CUP_30Rnd_556x45_PMAG_BLACK_RPL",7,30]]],["B_TacticalPack_blk",[["CUP_1Rnd_HEDP_M203",20,1],["CUP_30Rnd_556x45_PMAG_BLACK_RPL",5,30],[["CUP_launch_M72A6_Special","","","",["CUP_M72A6_M",1],[],""],2]]],"CUP_H_USA_Cap_NY_DEF","CUP_FR_NeckScarf2",["CUP_LRTV","","","",["Laserbatteries",1],[],""],["ItemMap","","ItemRadio","ItemCompass","ItemWatch",""]];

////////////////////////////////////////////////

_heli=createVehicle ["CUP_B_MH6J_USA",getpos player];
(units player) select (count (units player) - 2) moveInAny _heli;
(units player) select (count (units player) - 1) moveInAny _heli;


/*
[] spawn {
	sleep 5;
	{player setVariable["factionType","modern_russia_woodland"];AIDEBUG=True;_x  spawn convert;} forEach (units player - [player]);
};
*/