//Platoon
_SL="CUP_O_RU_Soldier_SL_M_EMR_V2"; 
_TL="CUP_O_RU_Soldier_TL_M_EMR_V2"; 
_GRE="CUP_O_RU_Soldier_GL_M_EMR_V2"; 
_R="CUP_O_RU_Soldier_M_EMR_V2"; 
_LAT="CUP_O_RU_Soldier_LAT_M_EMR_V2"; 
_AR="CUP_O_RU_Soldier_AR_M_EMR_V2"; 
_MG="CUP_O_RU_Soldier_MG_M_EMR_V2"; 
_MK="CUP_O_RU_Soldier_Marksman_M_EMR_V2"; 
_MED="CUP_O_RU_Soldier_Medic_M_EMR_V2"; 
_HAT="CUP_O_RU_Soldier_HAT_M_EMR_V2"; 
_MAT="CUP_O_RU_Soldier_AT_M_EMR_V2"; 

///////////////////////////////// 

platoonGroup = createGroup [side player, true]; 

//I put all the SL first in the hierarchy for easy selection 
_platoonComposition=[ 
  //Classname, rank, hierarchy in group, team 
  [_SL, "SERGEANT",2, "MAIN"], //PL SERGEANT 
  [_MED, "CORPORAL",3, "MAIN"], //PL Medic 
  [_MK, "CORPORAL",4, "MAIN"], //PL Marksman 
  [_R, "CORPORAL",5, "MAIN"], //PL Attachment 1 
  [_R, "PRIVATE",6, "MAIN"], //PL Attachment 2 

  //Squad leaders 
  [_TL, "SERGEANT",7, "RED"], //Security Squad Leader 
  [_SL, "SERGEANT",8, "GREEN"], //Assault 1 Squad Leader 
  [_SL, "SERGEANT",9, "BLUE"], //Weapons Squad Leader 

  //Security Squad 
  [_LAT, "CORPORAL",11, "RED"], //LAT 
  [_AR, "PRIVATE",12, "RED"], //AR 
  [_GRE, "PRIVATE",13, "RED"], //GRE 
  [_MAT, "PRIVATE",14, "RED"], //RPG 
  
  [_TL, "SERGEANT",15, "RED"], //Security Team Leader 
  [_LAT, "CORPORAL",16, "RED"], //LAT 
  [_AR, "PRIVATE",17, "RED"], //AR 
  [_GRE, "PRIVATE",18, "RED"], //GRE 
  [_MAT, "PRIVATE",19, "RED"], //RPG 

  //Assault 1 
  [_GRE, "CORPORAL",21, "GREEN"], //Grenadier 
  [_HAT, "PRIVATE",22, "GREEN"], //HAT 
  [_AR, "PRIVATE",23, "GREEN"], //AR 
  [_R, "PRIVATE",24, "GREEN"], //Rifleman 
  
  [_TL, "SERGEANT",25, "GREEN"], //Assault 1 Team Leader 
  [_GRE, "CORPORAL",26, "GREEN"], //Grenadier 
  [_HAT, "PRIVATE",27, "GREEN"], //HAT 
  [_AR, "PRIVATE",28, "GREEN"], //AR 
  [_R, "PRIVATE",29, "GREEN"], //Rifleman 


  //Weapons Squad 
  [_R, "CORPORAL",31, "BLUE"], //Rifleman 
  [_MG, "PRIVATE",32, "BLUE"], //MG 
  [_AR, "PRIVATE",33, "BLUE"], //AR 
  [_R, "PRIVATE",34, "BLUE"], //R 
  
  [_TL, "SERGEANT",35, "BLUE"], //Weapons Team Leader 
  [_R, "CORPORAL",36, "BLUE"], //Rifleman 
  [_MG, "PRIVATE",37, "BLUE"], //MG 
  [_AR, "PRIVATE",38, "BLUE"], //AR 
  [_R, "PRIVATE",39, "BLUE"] //R 
]; 


//////////////////////Spawning the platoon 

{ 
_x params ["_unitClassname","_unitRank","_orderInGroup","_myTeamName"]; 
_initString=format [ 
    "this setVariable ['ace_medical_damageThreshold',1000]; 
    this setSkill 1; 
    this disableAI 'AIMINGERROR'; 
    this setVariable ['lambs_danger_disableAI',true]; 
    this joinAsSilent [platoonGroup, %1]; 
    [this,'%2'] spawn { 
    _this params ['_myUnit','_myTeam']; 
    sleep 2; 
    _myUnit assignTeam _myTeam; 
    }; 
    doStop this;" 
  , _orderInGroup, _myTeamName]; 

  _unitClassname createUnit [ 
  getpos player, 
  platoonGroup, 
  _initString, 
  1, 
  _unitRank 
]; 
} forEach _platoonComposition; 


player joinAsSilent [platoonGroup, 1]; 
platoonGroup selectLeader player; 
player setRank "LIEUTENANT"; 

