TOV_spawn_motorized = {
  // [EAST, getpos player, "CUP_O_BMP2_RU" , "modern_russia_mixed" ] spawn TOV_spawn_motorized;
  //[EAST, getpos player, "CUP_O_BTR80A_DESERT_RU" , "modern_russia_mixed" ] spawn TOV_spawn_motorized;
  //[EAST, getpos player, "CUP_O_BTR80A_DESERT_RU" , "modern_russia_mixed" ] spawn TOV_spawn_motorized;
  
  params [["_side", WEST], ["_spawnPosition", getpos player], ["_vehicleType",""], ["_factionType",""]];

  //Platoon
  _SL="CUP_O_RUS_M_Soldier_SL_VKPO_Summer"; 
  _TL="CUP_O_RUS_M_Soldier_TL_VKPO_Summer"; 
  _GRE="CUP_O_RUS_M_Soldier_GL_VKPO_Summer"; 
  _R="CUP_O_RUS_M_Soldier_VKPO_Summer"; 
  _LAT="CUP_O_RUS_M_Soldier_LAT_VKPO_Summer"; 
  _AR="CUP_O_RUS_M_Soldier_AR_VKPO_Summer"; 
  _MG="CUP_O_RUS_M_Soldier_MG_VKPO_Summer"; 
  _MK="CUP_O_RUS_M_Soldier_Marksman_VKPO_Summer"; 
  _MED="CUP_O_RUS_M_Soldier_Medic_VKPO_Summer"; 
  _HAT="CUP_O_RUS_M_Soldier_HAT_VKPO_Summer"; 
  _MAT="CUP_O_RUS_M_Soldier_AT_VKPO_Summer"; 

  _ENG="CUP_O_RUS_M_Soldier_AT_Engineer_Summer";
  _EXP="CUP_O_RUS_M_Soldier_Exp_VKPO_Summer";

  ///////////////////////////////// 

  platoonGroup = createGroup [_side, true]; 

  //I put all the SL first in the hierarchy for easy selection 
  _platoonComposition=[ 
    //Classname, rank, hierarchy in group, team 
    //Security Squad 
    [_LAT, "CORPORAL",1, "WHITE"], //LAT 
    [_AR, "PRIVATE",2, "WHITE"], //AR 
    [_GRE, "PRIVATE",3, "WHITE"], //GRE 
    [_MAT, "PRIVATE",4, "WHITE"], //RPG 
    //Assault 1 
    [_GRE, "CORPORAL",5, "WHITE"] //Grenadier 
  ]; 


  //////////////////////Spawning the platoon 

  { 
  _x params ["_unitClassname","_unitRank","_orderInGroup","_myTeamName"]; 
  _initString=format [ 
      "this setVariable ['ace_medical_damageThreshold',1000]; 
      this setSkill 1; 
      this joinAsSilent [platoonGroup, %1]; 
      [this,'%2'] spawn { 
      _this params ['_myUnit','_myTeam']; 
      sleep 2; 
      _myUnit assignTeam _myTeam; 
      }; 
      " 
    , _orderInGroup, _myTeamName]; 

    _unitClassname createUnit [ 
    _spawnPosition, 
    platoonGroup, 
    _initString, 
    1, 
    _unitRank 
  ]; 
  } forEach _platoonComposition; 

  if (_vehicleType != "") then {
    _spawnedVehicle=[_spawnPosition findEmptyPosition [0,50,_vehicleType], 180, _vehicleType, platoonGroup] call BIS_fnc_spawnVehicle;
    _spawnedVehicle=_spawnedVehicle#0;
    
    _i=0;
    
    if (["BMP2_", _vehicleType] call BIS_fnc_inString ) then {_i=4};
    if (["BTR80A_", _vehicleType] call BIS_fnc_inString ) then {_i=0};
    
    
    { 
      if (vehicle _x==_x) then {
        _x assignAsCargoIndex [_spawnedVehicle, _i];
        _x moveInCargo [_spawnedVehicle, _i];
        _spawnedVehicle setUnloadInCombat [true, false];
        _i=_i+1;
      }
    } forEach units platoonGroup;
    
    platoonGroup selectLeader driver _spawnedVehicle;
    
  };
  
  if (_factionType!="") then {
    {factionType=_factionType; AIDEBUG=True;_x  spawn convert; save3DENInventory [_x];} forEach (units platoonGroup);
  };
  
  platoonGroup
}
