_side=WEST;
_factionType="modern_russia_mixed";

//Platoon
_SL="CUP_O_RUS_M_Soldier_SL_VKPO_Summer"; 
_TL="CUP_O_RUS_M_Soldier_TL_VKPO_Summer"; 
_GRE="CUP_O_RUS_M_Soldier_GL_VKPO_Summer"; 
_R="CUP_O_RUS_M_Soldier_VKPO_Summer"; 
_LAT="CUP_O_RUS_M_Soldier_LAT_VKPO_Summer"; 
_AR="CUP_O_RUS_M_Soldier_AR_VKPO_Summer"; 
_MG="CUP_O_RUS_M_Soldier_MG_VKPO_Summer"; 
_MK="CUP_O_RUS_M_Soldier_Marksman_VKPO_Summer"; 
_MED="CUP_O_RUS_M_Soldier_Medic_VKPO_Summer"; 
_HAT="CUP_O_RUS_M_Soldier_HAT_VKPO_Summer"; 
_MAT="CUP_O_RUS_M_Soldier_AT_VKPO_Summer"; 

_ENG="CUP_O_RUS_M_Soldier_AT_Engineer_Summer";
_EXP="CUP_O_RUS_M_Soldier_Exp_VKPO_Summer";

///////////////////////////////// 

platoonGroup = createGroup [side player, true]; 

//I put all the SL first in the hierarchy for easy selection 
_platoonComposition=[ 
  //Classname, rank, hierarchy in group, team 
  [_SL, "SERGEANT",2, "MAIN"], //PL SERGEANT 
  [_MED, "CORPORAL",3, "MAIN"], //PL Medic 
  [_MK, "CORPORAL",4, "MAIN"], //PL Marksman 
  [_R, "CORPORAL",5, "MAIN"], //PL Attachment 1 
  [_R, "PRIVATE",6, "MAIN"], //PL Attachment 2 

  //Squad leaders 
  [_TL, "SERGEANT",7, "RED"], //Security Squad Leader 
  [_SL, "SERGEANT",8, "GREEN"], //Assault 1 Squad Leader 
  [_SL, "SERGEANT",9, "BLUE"], //Weapons Squad Leader 

  //Security Squad 
  [_LAT, "CORPORAL",11, "RED"], //LAT 
  [_AR, "PRIVATE",12, "RED"], //AR 
  [_GRE, "PRIVATE",13, "RED"], //GRE 
  [_MAT, "PRIVATE",14, "RED"], //RPG 

  //Assault 1 
  [_GRE, "CORPORAL",21, "GREEN"], //Grenadier 
  [_HAT, "PRIVATE",22, "GREEN"], //HAT 
  [_AR, "PRIVATE",23, "GREEN"], //AR 
  [_R, "PRIVATE",24, "GREEN"], //Rifleman 

  //Weapons Squad 
  [_R, "CORPORAL",31, "BLUE"], //Rifleman 
  [_MG, "PRIVATE",32, "BLUE"], //MG 
  [_AR, "PRIVATE",33, "BLUE"], //AR 
  [_R, "PRIVATE",34, "BLUE"] //R 
]; 


//////////////////////Spawning the platoon 

{ 
_x params ["_unitClassname","_unitRank","_orderInGroup","_myTeamName"]; 
_initString=format [ 
    "this setVariable ['ace_medical_damageThreshold',1000]; 
    this setSkill 1; 
    this joinAsSilent [platoonGroup, %1]; 
    [this,'%2'] spawn { 
    _this params ['_myUnit','_myTeam']; 
    sleep 2; 
    _myUnit assignTeam _myTeam; 
    }; 
    " 
  , _orderInGroup, _myTeamName]; 

  _unitClassname createUnit [ 
  getpos player, 
  platoonGroup, 
  _initString, 
  1, 
  _unitRank 
]; 
} forEach _platoonComposition; 


currentGroup=platoonGroup;
{factionType="modern_russia_mixed";AIDEBUG=True;_x  spawn convert; save3DENInventory [_x];} forEach (units currentGroup);

