/*
To run in 3den:

{factionType="modern_russia_woodland";AIDEBUG=True;_x  spawn convert; save3DENInventory [_x];} forEach (get3DENSelected "object");

To run in debug for player Group:

{factionType="modern_russia_woodland";AIDEBUG=True;_x  spawn convert; save3DENInventory [_x];} forEach ((units player) - [player]);

*/


convert={
  //options : modern_russia_desertvdv, modern_russia_mixed, modern_russia_woodland
  params ["_unit"];
  _playerFaction=1;
  //_playerFaction=[(player getVariable "etr_var_group"), "factionType"] call etr_factions_fnc_getConfigAttribute;

  //for SP testing only
  if AIDEBUG then {	
	  //_playerFaction=player getVariable "factionType";
	  _playerFaction=factionType;
  };

  _uniforms=[];
  _vests=[];
  _helmets=[];
  _backpacks=[];
  _facewear=[];

  _specialRUVest=False;


  _ratnikVestReplacer={
	  params ["_unit"];

	  _currentVest=vest _unit;
	  _newVest=[_currentVest, "_Full", ""] call CBA_fnc_replace;
	  _newVest=[_currentVest, "_full", ""] call CBA_fnc_replace;
	  _newVest=[_newVest, "_Del", ""] call CBA_fnc_replace;	
	  _newVest
  };

  switch (_playerFaction) do {
      case "modern_russia_desertvdv": { 
			  _helmets=["CUP_H_RUS_6B27_cover_BeigeDigital","CUP_H_RUS_6B27_cover_BeigeDigital_goggles","CUP_H_RUS_6B27_cover_BeigeDigital_headset_goggles","CUP_H_RUS_6B27_cover_BeigeDigital_headset"];
			  _vests pushback ([_unit] call _ratnikVestReplacer);
		  };
	  case "modern_russia_mixed": { 
			  _helmets=["CUP_H_RUS_6B27_cover","CUP_H_RUS_6B46","CUP_H_RUS_6B27_cover_headset_goggles","CUP_H_RUS_6B27_cover_headset"];
			  _uniforms=["CUP_U_O_RUS_Soldier_VKPO_MSV_BeigeDigital_gloves_pads","CUP_U_O_RUS_Soldier_VKPO_MSV_BeigeDigital_gloves_pads","CUP_U_O_RUS_Soldier_VKPO_MSV_BeigeDigital_gloves_pads","CUP_U_O_RUS_Soldier_VKPO_MSV_BeigeDigital_rolled_up_gloves_pads"];
			  _facewear=[""];
			  _vests pushback ([_unit] call _ratnikVestReplacer);
		  };
	  case "modern_russia_woodland": { 
			  _helmets=["CUP_H_RUS_6B27_cover","CUP_H_RUS_6B46","CUP_H_RUS_6B27_cover_headset_goggles","CUP_H_RUS_6B27_cover_headset"];
			  _uniforms=["CUP_U_O_RUS_Soldier_VKPO_MSV_EMR_gloves_pads","CUP_U_O_RUS_Soldier_VKPO_MSV_EMR_gloves_pads","CUP_U_O_RUS_Soldier_VKPO_MSV_EMR_gloves_pads","CUP_U_O_RUS_Soldier_VKPO_MSV_EMR_rolled_up_gloves_pads"];
			  _facewear=[""];
			  _vests pushback ([_unit] call _ratnikVestReplacer);
		  };
		  
      default { };
  };


  //////////////////////////////////////////////////////////
  // Replacing the gear
  if ((count _helmets)>0) then {
	  if ((headgear _unit) !="")  then {
		  _newGear=selectRandom _helmets;
		  _unit addHeadgear _newGear;
	  };
  };

  if ((count _backpacks)>0) then {
	  if ((backpack _unit) !="")  then {
		  _oldItems = backpackItems _unit;
		  _newGear = selectRandom _backpacks;
		  removeBackpack _unit;
		  _unit addBackpack _newGear;
		  {_unit addItemToBackpack _x;} forEach _oldItems;
	  };
  };

  if ((count _vests)>0) then {
	  if ((vest _unit) !="")  then {
		  _newGear=selectRandom _vests;
		  _oldItems = vestItems _unit;
		  _unit addVest _newGear;
		  {_unit addItemToVest _x;} forEach _oldItems;		
	  };
  };

  if ((count _uniforms)>0) then {
	  _newGear=selectRandom _uniforms;
	  _oldItems = uniformItems _unit;
	  _unit forceAddUniform _newGear;
	  {_unit addItemToVest _x;} forEach _oldItems;		
  };

  if ((count _facewear)>0) then {
	  if ((goggles _unit) !="")  then {
		  removeGoggles _unit;
		  _newGear=selectRandom _facewear;
		  _unit addGoggles _newGear;
	  };	
  };
  //return
};
	
