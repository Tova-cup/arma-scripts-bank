_PIL="CUP_O_RU_Pilot_M_EMR";

_R="CUP_O_RUS_M_Soldier_VKPO_Summer";


/////////////////////////////////

platoonGroup = createGroup [west, true];

//I put all the SL first in the hierarchy for easy selection
_platoonComposition=[ 
	//Classname, rank, hierarchy in group, team
	
	//Pilots
	[_PIL, "LIEUTENANT",51, "MAIN"],			//Pilot
	[_PIL, "SERGEANT",52, "MAIN"],			//Co-pilot
	
	//Gunners
	[_R, "PRIVATE",53, "MAIN"],			//Security Squad Leader
	[_R, "PRIVATE",54, "MAIN"],			//Assault 1 Squad Leader
	[_R, "PRIVATE",55, "MAIN"]			//Weapons Squad Leader
];



//////////////////////Spawning the platoon

{
	_x params ["_unitClassname","_unitRank","_orderInGroup","_myTeamName"];
	_initString=format [
		"this setVariable ['ace_medical_damageThreshold',1000];
		this setSkill 1;
		this disableAI 'AIMINGERROR';
		this setVariable ['lambs_danger_disableAI',true];
		this joinAsSilent [platoonGroup, %1];
		[this,'%2'] spawn { 
			_this params ['_myUnit','_myTeam'];
			sleep 2;
			_myUnit assignTeam _myTeam;
			};
		doStop this;"
	, _orderInGroup, _myTeamName];

	_unitClassname createUnit [
		getpos player,
		platoonGroup,
		_initString,
		1,
		_unitRank
	];
} forEach _platoonComposition;


//player joinAsSilent [platoonGroup, 1];
//platoonGroup selectLeader player;
//player setRank "LIEUTENANT";

//Helicopter///////////////////
_heli = createVehicle ["CUP_B_Mi171Sh_ACR",getpos player,[],0,"NONE"];
[
	_heli,
	//["Soviet_KGB_8MT",1], 
	["CIV_TAN_BLUE",1], 
	true
] call BIS_fnc_initVehicle;



(units platoonGroup)#0 moveInDriver _heli;
(units platoonGroup)#1 moveInTurret [_heli,[3]];
{
	if (vehicle _x == _x) then {
		_x moveInAny _heli;
	};
} forEach units platoonGroup;


//Joining the player group
{
	//finding unit ID pos in the Group
	_str = str _x;
	_unitIdPos=parseNumber (_str select [(_str find ":") + 1]);
	
	//joining the storing group
	_x joinAsSilent [group player, _unitIdPos];
	dostop _x;
} forEach (units platoonGroup);

/*
[] spawn {
	sleep 5;
	{player setVariable["factionType","modern_russia_woodland"];AIDEBUG=True;_x  spawn convert;} forEach (units player - [player]);
};
*/
