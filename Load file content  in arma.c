//Load file content
[] spawn {
  TOV_gen_text=True;
  TOV_can_speak=True;

  toSay=[];
  _lastSaid=-1;

  while {TOV_gen_text} do {

    if !(fileExists format ["\z_sideMissions\convs\conv%1.txt",_lastSaid+1]) then {
      continue;
    }; 
    
      _currentLine = loadFile format ["\z_sideMissions\convs\conv%1.txt",_lastSaid+1];
    if ((count toSay == 0) or {(toSay select -1) != _currentLine}) then {
      toSay pushback _currentLine;
    };
    
    hint str _lastSaid;
    
    if ((_lastSaid < (count toSay) -1) and TOV_can_speak) then {
      TOV_can_speak=False;
      _lastSaid=_lastSaid+1;
      (toSay select _lastSaid) remoteExec ["systemChat",0] ;
      //[civilian, "HQ"] sideChat (toSay select _lastSaid);
      [] spawn {
        sleep 15;
        TOV_can_speak=True;
        };
    };
    
    publicVariable "toSay";
    sleep 0.5;

  };
};
