//quick play config :
(group this) setVariable ["Vcm_Disable",true];
(group this)  enableAttack false;
(group this) allowFleeing 0;

//group player
(group this) allowFleeing 0;
{_x setSkill ["courage", 1];_x setSkill ["commanding", 1]; } foreach (units group this); 

//basic SP AI quickplay
_myUnit=player;
leader _myUnit allowSprint false;
(group _myUnit)  enableAttack false;
{_x setSkill 1; } foreach (units _myUnit); 
 
//night
{
	 _x setUnitTrait ["camouflageCoef", 0.01]; 
	 _x setUnitTrait ["audibleCoef", 0.03]; 
	 _x setSkill ["spottime", 0.01]; 
} foreach allunits;

{_x assignItem "rhsusf_ANPVS_14"} foreach units player;
execVM "badNVG.sqf";
(group player) enableIRLasers true;

//add NVG
{_x linkItem "rhs_1PN138";} foreach units group player;


//patrol with search
[this, this, 300, 7, "MOVE", "SAFE", "YELLOW", "NORMAL", "STAG COLUMN", "this call CBA_fnc_searchNearby", [3, 6, 9]] call CBA_fnc_taskPatrol;
//patrol
[this, this, 300, 7, "MOVE", "SAFE", "YELLOW", "LIMITED", "COLUMN", "", [3, 6, 9]] call CBA_fnc_taskPatrol;

//if stealth and spotted how to revert :
[] spawn {
	while {true} do {
		waitUntil {(combatMode group player)=="YELLOW"};
		group player setBehaviour "AWARE"; comment "//should automatically switch to combat";
		waitUntil {behaviour player=="AWARE"};
		group player setCombatMode "GREEN";
	};
};

//spotter get distance to target:
player addAction ["Get distance",{
	 _ins = lineIntersectsSurfaces [AGLToASL positionCameraToWorld [0,0,0],AGLToASL positionCameraToWorld [0,0,5000],vehicle player,objNull,true,1,"FIRE","NONE"];
	_cursor_distance = if (count _ins > 0) then [{(_ins select 0 select 0) vectorDistance (eyepos player)},{5000}];
	_aim_dist_txt = if (count _ins > 0) then [{str(round _cursor_distance)},{"---"}];
	u groupChat format["Target is at %1 m.",_aim_dist_txt];
},[],10.5,false,false ]


//splendid camera from sqs camera
while {true} do {
	waitUntil {
		uiSleep 1;
		not isNull findDisplay 314;
	};

	findDisplay 314 displayAddEventHandler ["KeyUp", {
		params ["_displayorcontrol", "_key", "_shift", "_ctrl", "_alt"];
		
		if (_key isEqualTo 46 && _ctrl) exitWith {
			_camParameters=call compile copyFromClipboard;
			_targetPos=screenToWorld [0.5, 0.5];

			_camOutput = Format ["
%1 camPrepareTarget %2;
%1 camPreparePos %3;
%1 camPrepareFOV %4;
//setAperture %5;
//%1 setDir %8;
//[%1, %6, %7] call BIS_fnc_setPitchBank;
%1 camCommitPrepared 0;
//waitUntil {camCommitted %1};
",
"_camera",_targetPos,_camParameters select 1,_camParameters select 3,_camParameters select 6,_camParameters select 4 select 0,_camParameters select 4 select 1,_camParameters select 2
			];

			copyToClipboard _camOutput;
		};
	}];

	waitUntil {
		uiSleep 1;
		isNull findDisplay 314;
	};

}


//command to get rid of rabbits and animals sounds
EnableEnvironnement 

//The Twins - an anomaly script

//Effects maker : https://steamcommunity.com/sharedfiles/filedetails/?id=350606620

// Use orbat creator for factions

//Abandoned road full with litter : http://www.armaholic.com/page.php?id=34455
// change this line to give pos and radius to create junk around 
private _roads = [worldSize/2,worldSize/2] nearRoads worldSize;

//get known enemy targets/units 
leader player targets [true, 900, [resistance],300];

//to check if plyaer is looking at something
cursorTarget or cursorObject : 

//Inspect leaflet as task (task end on leaflet read) : https://forums.bohemia.net/forums/topic/215839-inspect-leaflet-as-task/?tab=comments#comment-3281330


//Make units take 2 times less damage (to put in init field) : 
this addEventHandler ["HandleDamage", {((_this select 2)/2)}];

//artillery script to shoot at an area that's a trigger :
[] spawn {for "_i" from 1 to 10 do {{_x doArtilleryFire [area call BIS_fnc_randomPosTrigger,"12Rnd_230mm_rockets",1]; sleep 3;} forEach units group tt;};};
//Keep firing with random spacing
[] spawn {
	while {true} do {
		sleep random 10;
		{_target=area call BIS_fnc_randomPosTrigger; _x setVehicleAmmo 1; _x doWatch _target;_x doArtilleryFire [_target,"CUP_30Rnd_122mmHE_D30_M",1]; sleep random 3;} forEach units group tt;
	};
};
    
//damage buildings : 
{_x setdammage ((random 0.5)+0.4)} foreach ((getpos this) nearObjects ["House", 500]);
//or 
[center, 300] call BIS_fnc_destroyCity;

//course poursuite : https://community.bistudio.com/wiki/setDriveOnPath
setDriveOnPath

//Add skip to conversation : in radioCall script I made add interaction with skip, if skipped, then end the function. (don't forget to make the skip a parameter, or make two functions)

//Hint with prompt : https://community.bistudio.com/wiki/hintC
hintC

//commands to transfert loadouts : 
getUnitLoadout and saveVar //saveVar makes a variable gloabal for all missions


//get player stance to order others to copy : https://community.bistudio.com/wiki/stance use with waitUntil because no eventHandler and https://community.bistudio.com/wiki/setUnitPos
    
//keep units in vehicles underfire : 
setUnloadInCombat

//Force AI to keepformation at all costs
{_x disableAI "TARGET"} forEach units group player

//Change animation speed (slow walk) not working with ACE unless onEachFrame: 
setAnimSpeedCoef

//SetDriveOnPath and unit capture for cars : 

[] spawn { 
	pathArray = []; 
	_lastPos=[0,0,0]; 
	while {true} do { 
		waitUntil {(getPosATL (vehicle player) distance _lastPos) > 5};
		_newPos=getPosATL (vehicle player); 
		pathArray pushBack _newPos; 
		_lastPos=_newPos; 
	}; 
}
//Manual record :
pathArray = []; 
player addAction ["record point",{pathArray pushback (getpos player)},[],10.5,true,false ]

//to find name of doors for heli or building :
// view in config viewer--> nameOfTheItem -->userActions

//Agent when dealing with use agent : example : 
{AGENT _x moveTo position player} forEach agents;
    
//lauch from carrier : 
BIS_fnc_AircraftCatapultLaunch

//Plank - A simple, but powerful fortification deployment script 

//Jebus respawn : https://forums.bohemia.net/forums/topic/174661-jebus-just-editor-based-unit-spawning/

//SHK_Fastrope 

//guitar mod + KJ - CHEEKI BREEKI full version (Guitar cover) [HD]
    
// Attach to guide : http://killzonekid.com/arma-scripting-tutorials-attachto-and-setvectordirandup/

//spawning animals would be cool

//GF blood stains

//set ambiant civilian identity (in init field of module):
//in init field of module
_faces=["PersianHead_A3_01","PersianHead_A3_02","PersianHead_A3_03"];
_voices=["Male01PER","Male02PER","Male03PER"];
_midEastUniforms=["CUP_O_TKI_Khet_Jeans_04","CUP_O_TKI_Khet_Jeans_02","CUP_O_TKI_Khet_Jeans_01","CUP_O_TKI_Khet_Jeans_03","CUP_O_TKI_Khet_Partug_04","CUP_O_TKI_Khet_Partug_02","CUP_O_TKI_Khet_Partug_01","CUP_O_TKI_Khet_Partug_07","CUP_O_TKI_Khet_Partug_08","CUP_O_TKI_Khet_Partug_05","CUP_O_TKI_Khet_Partug_06","CUP_O_TKI_Khet_Partug_03"];

_this setFace (selectRandom _faces);
_this setSpeaker (selectRandom _voices);
_this forceAddUniform (selectRandom _midEastUniforms);
_this setAnimSpeedCoef 0.7;

//spawn many civs in a market :
[] spawn {
	_faces=["PersianHead_A3_01","PersianHead_A3_02","PersianHead_A3_03"];
	_voices=["Male01PER","Male02PER","Male03PER"];
	marketCivs=[];
	for "_i" from 1 to 20 do{
		_dude=createAgent [format["C_man_polo_%1_F",round(1+random 5)], [["market_marker"]] call BIS_fnc_randomPos, [], 0, "NONE"];
		_dude disableAI "FSM";
		_dude setDir random 360;
		_dude setFace (selectRandom _faces);
		_dude setSpeaker (selectRandom _voices);
		marketCivs=marketCivs + [_dude];
		sleep random 2;
	};
};

//Jukebox script
//"initialize": [stealthMusics, combatMusics, safeMusics, volume, transition, radius, executionRate, noRepeat] 
["initialize",[
	//stealth ↓
	["EventTrack01a_F_Tacops"],
	//combat ↓
	["LeadTrack04_F_EPC","LeadTrack05_F_Tank","LeadTrack01_F_Tank","MainTheme_F_Tank","LeadTrack02_F_Mark","BackgroundTrack03_F"],
	//safe ↓
	["EventTrack01a_F_Tacops"],
	//volume ↓
	0.2, 5, 500,5,true ]
] call BIS_fnc_jukebox;

//Use ALias fire, you can put helicopters in fire as well ==> http://www.armaholic.com/page.php?id=30780

//dust storm : https://youtu.be/lB1Z1xJKBYI

//dust and floating particles : https://youtu.be/JTP38ZIyHqo

//check how units movement is done 
https://steamcommunity.com/sharedfiles/filedetails/?id=1760343236

//item user texture in the editor, and setTexture for cool floating text :
setObjectTexture [0, "rsc\ring\r1.paa"];

//use strategic map module for cool mission selection

//Access layer items, returns : [objects:Array, markers:Array] :
getMissionLayerEntities "my Layer" select 0

//To down a heli a car, or kill a foot unit (with a mine) elegantly use : 
BIS_fnc_neutralizeUnit

//GOM AA fire : https://forums.bohemia.net/forums/topic/197937-release-gom-ambient-aa-v121/

//stay in no matter what : https://community.bistudio.com/wiki/allowCrewInImmobile
allowCrewInImmobile

//Change side : 
joinSilent createGroup EAST;

//Calculate some path
https://community.bistudio.com/wiki/calculatePath

//ruin making
https://community.bistudio.com/wiki/BIS_fnc_createRuin

//to script NVG on :
{_x assignItem "rhsusf_ANPVS_14"} foreach units group player;

//No damage on friendly vehicle collision : 
http://www.armaholic.com/page.php?id=27485

//use showUAVFeed to show UAV cam on laptop
showUAVFeed

//Ace view distance
currentViewDistance = viewDistance;
private _self_viewdistance = ['ViewDistance','Set view distance','',{hint format["Current view distance: %1", currentViewDistance];},{true}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions"], _self_viewdistance] call ace_interact_menu_fnc_addActionToObject;

{
	private _self_viewvalue = [format["view%1",_x],format["%1",_x],'',compile format["currentViewDistance=%1;setViewDistance %1;setObjectViewDistance %1;", _x],compile format["%1!=currentViewDistance",_x]] call ace_interact_menu_fnc_createAction;
	[player, 1, ["ACE_SelfActions", "ViewDistance"], _self_viewvalue] call ace_interact_menu_fnc_addActionToObject;
} forEach [500,1000,1500,2000,2500,3000,3500,4000,4500,5000,6000,7000,8000,9000,10000,20000];

// down a heli : 
{_x disableAI "ALL"} foreach crew v;
v setUnloadInCombat [FALSE,FALSE];   
vD setDammage 1;
v allowCrewInImmobile true;
v lock true;
v setDammage 0.8;
v setVelocityModelSpace [0,0, -1];

//Description.ext HUB example : https://forums.bohemia.net/forums/topic/155441-the-new-campaign-descriptionext/
//MERCS campaign is also a good working example 
//How to end missions :
["epicFail", false, 2] call BIS_fnc_endMission;

//Save and getloadout : If you are using ACE3 you can use ACE_common_fnc_getAllGear and ACE_common_fnc_setAllGear

//Use activateKey to lock/unlock missions, a good trick to nevershow the VR missions on the campaign list https://community.bistudio.com/wiki/activateKey
//Actually this makes no sense for campaigns
activateKey "Mission04Key";

//Use to remove corpse from garbage collector : https://community.bistudio.com/wiki/removeFromRemainsCollector
//Maybe run it a couple seconds after mission launch, just to be sure
removeFromRemainsCollector [unit1, unit2, vehicle1];

//attach camera to moving object : https://forums.bohemia.net/forums/topic/183259-securely-attaching-camera-to-uav/

//-To disable simulation, place in init : 
[this] spawn {
	sleep 2;
	_this select 0 enableSimulation false;
}

//Spawn an OPFOR laser target, the AttachTo is important , otherwise the laser disappears after 40 seconds ! This is the target: 
_laserTarget = "LaserTargetE" createVehicle getPosATL this; 
_laserTarget attachTo [this, [0, 0, 3]];

/*Spawn CAS module wit h script : https://forums.bohemia.net/forums/topic/167654-cas-on-More
More details on parameters, see page 2 for example with moving target : https://forums.bohemia.net/forums/topic/166474-how-to-use-modulecas_f/ mapclick/ */

//To create new map tabs : https://community.bistudio.com/wiki/createDiaryRecord
player createDiaryRecord ["Diary", ["Intel", "Enemy base is on grid <marker name='enemyBase'>161170</marker>"]]

//Vanilla Flares are bad for illumination, but here’s a quick code:
_flrObj = "F_40mm_White" createvehicle ((player) ModelToWorld [(random 60)-30,(random 60)-30,200]);  
_flrObj setVelocity [-1+random 2,-1 + random 2,-0.1-random 0.5];
//combine with this for good effect insead : http://www.armaholic.com/page.php?id=31763
//Or check GOM flare fix, I think it whouldbe better : https://forums.bohemia.net/forums/topic/213604-flare-illumination/

//To force quick move after engagement, use trigger : if no enemies alive at engagement zone, skip waypoint, setBeahaviour Aware (maybe a temporarty disable AUTOCOMBAT as well), that should force the squad to move to next waypoint

//To force panic on spawned civvies, put in their init field switchmove “panicAnimation”

//to move quick under fire, set behaviour to AWARE and disable AUTOCOMBAT+TARGET, combine with a FiredNear EH to trigger combat behaviour and revert to AWWARE if no shot for 5 seconds

//delete vehicle and crew on waypoint completion :
{deleteVehicle _x} forEach crew vehicle this + [vehicle this];

//Play videos with : https://community.bistudio.com/wiki/BIS_fnc_playVideo
//Could use/alter the source code to display an image
BIS_fnc_playVideo

//To make a unit able to defuse mines + other lines for better effect: 
this setUnitTrait ["explosiveSpecialist",true];
this setUnitTrait ["explosiveSpecialist",true];
this forceWalk true;
this allowSprint false;

//Aim at something and force firing
_this dotarget targ;
sleep 4;
_this fire (currentWeapon _this)};

//unkillabe units when using ACE -- must enable prevent instant death
["ace_unconscious", {
	params ["_unit","_state"];
		if (_state) then {
			if (_unit in unkillable) then {_unit allowDamage false}
			else {if (random 1 <0.75) then {_unit setdamage 1}};
		} else {_unit allowDamage true} ;
}] call CBA_fnc_addEventHandler;

//weapon on back
player action ["SwitchWeapon", player, player, 100];

//ACE unkillable on unconscious
["ace_unconscious", {
	params ["_unit","_state"];
	_unkillable=[a,b];
	if (_unit in _unkillable) then {
		if (_state) then {_unit allowDamage false} else {_unit allowDamage true};}
	else {_unit setdamage 1};	
	//hint format ["this %1 is now %2",_unit,_state];
}] call CBA_fnc_addEventHandler;

//To draw a 3D text oversomething in game use : https://community.bistudio.com/wiki/drawIcon3D
addMissionEventHandler ["Draw3D", {	
	_pos = getPosWorld player;
	drawIcon3D ["a3\ui_f\data\gui\Rsc\RscDisplayArsenal\radio_ca.paa", [1,1,1,1], [(_pos select 0),(_pos select 1), 1], 0.8, 0.8, 0, (name player), 1, 0.0315, "EtelkaMonospacePro"];
}];


//Create a custom radio menu https://community.bistudio.com/wiki/showCommandingMenu
//https://forums.bohemia.net/forums/topic/80785-comms-and-custom-menues/?do=findComment&comment=1386836
showCommandingMenu

//Prevent ACE instant death on a single unit :
_unit setVariable ["ace_medical_preventInstaDeath",true];


//Randomly crouch while fighting from window

vault=true;
while {vault} do {
	if (random 1 < 0.5) then {
		{
			_x setUnitPos "MIDDLE";
			sleep (4+random 5);
			_x setUnitPos "UP";
		} forEach units group player;
	};
	sleep 10;
};

//Attach the carrier to something, use the link from the post not armaholic, don't forget to use createVehicle instead of simpleObject
https://forums.bohemia.net/forums/topic/204713-polpoxs-attach-uss-freedom-script/
//To combine with 
https://forums.bohemia.net/forums/topic/204602-wmo-walkable-moving-objects/
//or
https://forums.bohemia.net/forums/topic/160566-attachtowithmovement-walkable-vehicle-interriors-and-exterirors/
https://forums.bohemia.net/showthread.php?157367-The-answer-you-were-waiting-for-carriers-are-possible-in-arma-3&p=2426734&viewfull=1#post2426734


//particle maker
http://www.armaholic.com/page.php?id=34755

//Defend command :
//[this,"INIT=", "[_proxyThis, _proxyThis, 400, 2, 0.1, 0.1 ] call CBA_fnc_taskDefend"] spawn jebus_fnc_main; 
/*
_group      - the group <GROUP, OBJECT>
    _position   - centre of area to defend <ARRAY, OBJECT, LOCATION, GROUP> (Default: _group)
    _radius     - radius of area to defend <NUMBER> (Default: 50)
    _threshold  - minimum building positions required to be considered for garrison <NUMBER> (Default: 3)
    _patrol     - chance for each unit to patrol instead of garrison, true for default, false for 0% <NUMBER, BOOLEAN> (Default: 0.1)
    _hold       - chance for each unit to hold their garrison in combat, true for 100%, false for 0% <NUMBER, BOOLEAN> (Default: 0)
*/
[this, this, 300, 2, 0.1, 0.1 ] call CBA_fnc_taskDefend

//Detcord demining mines
https://forums.bohemia.net/forums/topic/202868-mine-clearing-detcord/

//Jukebox for hot assault
//"initialize": [stealthMusics, combatMusics, safeMusics, volume, transition, radius, executionRate, noRepeat] 
["initialize",[
	//stealth ↓
	["EventTrack01a_F_Tacops"],
	//combat ↓
	["LeadTrack04_F_EPC","LeadTrack05_F_Tank","LeadTrack01_F_Tank","MainTheme_F_Tank","LeadTrack02_F_Mark","BackgroundTrack03_F","LeadTrack04_F_EXP"],
	//safe ↓
	["EventTrack01a_F_Tacops","EventTrack03a_F_Tacops","EventTrack03b_F_Tacops",],
	//volume ↓
	0.5, 5, 500,5,true ]
] call BIS_fnc_jukebox;

//How to play video on those weird data terminals https://www.youtube.com/watch?v=ftoCWo0LoIs

//Command to retrive the headmounted item :  https://community.bistudio.com/wiki/hmd
hmd

//switch uniform and keep the items
//Variant for vest with vestItems 
_myUnit=_this;
_newUniform="rhs_uniform_emr_des_patchless";
_oldUniformItems = uniformItems _myUnit;
_myUnit forceAddUniform _newUniform;
{_myUnit addItemToUniform _x;} forEach _oldUniformItems;

//Good ambient animation array : https://community.bistudio.com/wiki/BIS_fnc_ambientAnimCombat
/*sleep is necessary otherwise the unit freezes on mission start*/
//Parameters : the unit, and the anim. Choose from : ["STAND","SIT_LOW","LEAN","WATCH","WATCH1","WATCH2"]
[this] spawn {
	params ["_unit",["_anim", selectRandom ["STAND","SIT_LOW","WATCH","WATCH1","WATCH2"] ]];
	sleep random 2;
	[_unit, _anim, "ASIS", {false},"AWARE"] call BIS_fnc_ambientAnimCombat;
};

//Or
[this] spawn {
	params ["_unit",["_anim", selectRandom ["STAND1","STAND_U1","STAND_U2","STAND_U3","WATCH","WATCH2","GUARD"] ]];
	if (local _unit) then { 
	    _unit disableAI "MOVE";
	    sleep random 2;
	    [_unit, _anim, "ASIS"] call BIS_fnc_ambientAnim;
	    _unit enableAI "MOVE";
	    waitUntil {sleep 2; (behaviour _unit == "COMBAT") or (!(alive _unit))};
	    _unit call BIS_fnc_ambientAnim__terminate;
        };
};

"PRONE_INJURED_U1", "PRONE_INJURED_U2", "PRONE_INJURED"

//For convoys : don't forget to disable AUTOCOMBAT+TARGET if you want to push through umbushes
this setConvoySeparation 50;
this limitSpeed 50;
//a loop to ensure the convoy doesn't get stuck
_convoyGroup=group this;
_convoyUnsticker = [_convoyGroup] spawn { 
	params ["_convoyGroup",["_convoySpeed",50],["_convoySeparation",50]];
	_convoyGroup setFormation "COLUMN";
	{(vehicle _x) limitSpeed _convoySpeed*1.15} forEach (units _convoyGroup);
	(vehicle leader _convoyGroup) limitSpeed _convoySpeed;
	while {sleep 10;true} do { 
		{
			(vehicle _x) setConvoySeparation _convoySeparation;
			if (speed vehicle _x < 5) then {
				(vehicle _x) doFollow (leader _convoyGroup);
			};	
		} forEach (units _convoyGroup)-(crew (vehicle (leader _convoyGroup)))-[player]; 
	}; 
};
//to stop the unsticker script :
teminate _convoyUnsticker;

//check the KK example for top down view : https://community.bistudio.com/wiki/switchCamera
player switchCamera "Gunner";

//Scripted ACE injuries -----------> broken !
//check https://github.com/acemod/ACE3/issues/2816 ---->thojkooi comment
//legs injury vanilla : player setHitPointDamage ["hitLegs", 0.5];
_bodypart = ["head", "body", "hand_l", "hand_l", "hand_r", "leg_l", "leg_r"] call BIS_fnc_selectRandom;
			_Size = 0.2 + random 0.6;
			//_amount = 1 + round random 4;
			_WoundType = ["bullet", "grenade", "explosive", "shell", "vehiclecrash", "backblast", "stab", "punch", "falling", "unknown"] call BIS_fnc_selectRandom;	
			[_victim, _Size, _bodypart, _WoundType] call ace_medical_fnc_addDamageToUnit;
	
//Could use this to order helicopter to target something https://community.bistudio.com/wiki/targetsQuery	

//Move inside buildings
//record with :
/*
	pathArray = []; 
	player addAction ["record point",{pathArray pushback (getposATL player)},[],10.5,true,false ]
*/
_unit= units group player select 1;
_pathArray = pathArray;
[_unit,_pathArray] spawn {
	params ["_unit","_pathArray"];
	{
		_unit doMove _x;
		waitUntil {sleep 1; (unitReady _unit) || ( _unit distance _x < 2)};
	}forEach _pathArray;
};
//copyToClipboard str pathArray;	
[_unit,_pathArray] spawn {
	params ["_unit","_pathArray"];
	{	
		_unit doMove _x;
		_loopsUntilStuck=7;
		_oldPos=getPosAtl _unit;
		while {true} do {
			sleep 1;
			if ((unitReady _unit) || ( _unit distance _x < 1.5)) exitWith{};
			if ((_unit distance _oldPos<0.5) && (_loopsUntilStuck<0))  exitWith{}; 
			_oldPos=getPosAtl _unit;
			_loopsUntilStuck=_loopsUntilStuck-1;
		};
	}forEach _pathArray;
};

//Turn off street lights around object
{ 
_x setHit ["light_1_hitpoint", 0.97]; 
_x setHit ["light_2_hitpoint", 0.97]; 
_x setHit ["light_3_hitpoint", 0.97]; 
_x setHit ["light_4_hitpoint", 0.97]; 
} forEach nearestObjects [someObjectName, [ "Lamps_base_F", "PowerLines_base_F", "PowerLines_Small_base_F" ], 500];

//Hide map indicators  https://community.bistudio.com/wiki/disableMapIndicators
disableMapIndicators [true,true,false,false];

//force light or laser on, works also for individual unts :
{_x  enableGunLights "forceOn"} foreach units group this;
{_x  enableIRLasers true} foreach units group this;

//add flash lights to the guns
this addPrimaryWeaponItem "acc_flashlight";
//or
{_x addPrimaryWeaponItem "acc_flashlight"; _x  enableGunLights "forceOn"} foreach units group this;


// Change side for stuff
_SideHQ = createCenter east; // you may or may not need to create the sides center depending if the side in quetion hs already been defined. ie; an east unit placed via editor, then this is not needed
grpName = createGroup east﻿;
{[_x] joinSilent grpName;﻿} forEach units addonGrpName;

//Animate/animation unit in editor
u=get3DENSelected "object" select 0;
u enableSimulation true;
u switchMove "passenger_apc_generic03"

//Enable AIS revive for player squad
{[_x] call AIS_System_fnc_loadAIS } forEach units group player

//liberation get my squad
units myPlatoon joinSilent Player;
{_x setpos getpos Player; _x enableSimulation true} forEach units group player;
{[_x] call AIS_System_fnc_loadAIS } forEach units group player;
{units group player select _x assignTeam "YELLOW"} foreach [1,2];
{units group player select _x assignTeam "RED"} foreach [3,4,5,6];
{units group player select _x assignTeam "GREEN"} foreach [7,8,9,10];
{units group player select _x assignTeam "BLUE"} foreach [11,12,13,14];
// on end of game
{deleteVehicle _x} foreach units group player -[player]


//Civilian module force panic mode, to put in the in spawn field:
if (random 1<0.5) then {
	_this switchMove "ApanPknlMwlkSnonWnonDf";
} else {
	_this switchMove "ApanPercMrunSnonWnonDf";
}

//unequip NVGs
{ _x unassignItem (hmd _x); } foreach units group player; 
//{ _x unassignItem "NVGoggles_INDEP"; } foreach units group player; 

//Switch to russian desert uniform
{	
	_myUnit=_x;
	if (side _myUnit == east) then {
		_newUniform="rhs_uniform_emr_des_patchless";
		_oldUniformItems = uniformItems _myUnit;
		_myUnit forceAddUniform _newUniform;
		{_myUnit addItemToUniform _x;} forEach _oldUniformItems;
		
		if (vest _myUnit == "rhs_6b23_rifleman") then {
			_newVest="rhs_6b23_6sh92";
			_oldVestItems = VestItems _myUnit;
			_myUnit AddVest _newVest;
			{_myUnit addItemToVest _x;} forEach _oldVestItems;
		};

	};
} forEach allUnits;

//Switch to cooler UCP uniform
{	
	_myUnit=_x;
	if (side _myUnit == west) then {
		_newUniform="rhs_uniform_acu_ucp";
		_oldUniformItems = uniformItems _myUnit;
		_myUnit forceAddUniform _newUniform;
		{_myUnit addItemToUniform _x;} forEach _oldUniformItems;
	};
} forEach allUnits;

//Notification system
http://www.armaholic.com/page.php?id=35258

//amazing metro map 
https://steamcommunity.com/sharedfiles/filedetails/?id=1821000791

//cool factory
https://steamcommunity.com/sharedfiles/filedetails/?id=1811259805

//lock doors
//Check this also : https://gist.github.com/AgentRev/6753309cfea02f3cc046
_num = getNumber(configfile >> "CfgVehicles" >> typeOf (nearestBuilding player) >> "numberOfDoors");
for "_i" from 0 to _num do
{
	(nearestBuilding player) setVariable [format ["bis_disabled_door_%1", _i], 1, true];
};


//Disable ACE Medic auto-heal other people
{
	if ((_x getUnitTrait "medic") and (_x getVariable ["ace_medical_medicClass",0] != 0)) then {
		_x setVariable ["ace_medical_medicClass", 0];
	} else 
	{
		if (_x getUnitTrait "medic") then { _x setVariable ["ace_medical_medicClass", 1]};
	};
} forEach ((units player) select {!(isPlayer _x)}) ;

//ACE3 Basic Full heal
[objNull, player] call ace_medical_treatment_fnc_fullHeal;

//Play anim on unit in 3DEN
_unit=get3DENSelected "object" select 0;
_unit enableSimulation true;
_unit switchMove "HubSittingChairB_idle1";

//Inspiration for notepad
https://github.com/Saborknight/9liners-and-notepad/


//Attack cba script
//
//[this,"INIT=", "_dest=getMarkerPos 'AO'; group _proxyThis addWaypoint [_dest, 400]; [_proxyThis, _dest, 400, false] call CBA_fnc_taskAttack"] spawn jebus_fnc_main; 
group this addWaypoint [getMarkerPos "tov_target", 400];
[this, getMarkerPos "tov_target", 400, false] call CBA_fnc_taskAttack

//Waypoint + attack
_currentGroup=this;
_markerName="tov_target";
_position=getMarkerPos ((allMapMarkers select { markerText _x  == _markerName})#0);
[_currentGroup, _position, 100, "MOVE", "AWARE", "RED", "NORMAL", "WEDGE", "[this, this, 400, false] call CBA_fnc_taskAttack"] call CBA_fnc_addWaypoint;

//Attack cba script
[this, getMarkerPos "tov_target", 400, false] call CBA_fnc_taskAttack

//cave system
https://steamcommunity.com/sharedfiles/filedetails/?id=1687651504

//strobe
{
  _light_obj = "NVG_TargetC" createVehicle [0,0,0];
  _light_obj attachTo [_x, [0,-0.03,0.07], "LeftShoulder"];
} forEach units group player 


//Scripted infantry movement with animation
_unit= d;
_pathArray = [[4680.4,6175.85,0.00143909],[4680.3,6170.7,0.00143909],[4687.46,6170.8,0.00143337],[4687.41,6173.25,0.00143242],[4681.15,6172.98,0.00143909],[4681.09,6170.27,0.00143909],[4673.9,6169.54,0.00143909],[4672.35,6164.15,0.00143909],[4665.02,6163.57,0.00143909]];
[_unit,_pathArray] spawn {
	params ["_unit","_pathArray"];
	{
		_direction= _unit getdir _x;
		while {sleep 0.5; ( _unit distance _x > 1)} do {
			_unit setDir _direction;
		}
	}forEach _pathArray;
};
//V2
params ["_unit","_pathArray"];
	{
		_direction= _unit getdir _x;
		_unit setDir _direction;
		waitUntil {sleep 0.1;( _unit distance _x < 1)}
	}forEach _pathArray;
};

//how to sink ships
https://www.youtube.com/watch?v=HalYxb8peWw

//To follow player, 3m is good
u=[r,3] spawn {
	params ["_unit","_howClose"];
	_oldPos=getPosATL player;
	_currentDestination=getPosATL player;
	//_unit doMove _oldPos;
	while {sleep 0.5; true} do {
		switch (vehicle player) do {
			case player : {
				_playerPos=getPosATL player;
				if ((getPosATL _unit) distance _playerPos < _howClose) then {doStop _unit};
				if (
						(_unit distance _playerPos > 20) AND
						([objNull, "VIEW"] checkVisibility [eyePos player, eyePos _unit]<0.3) AND
						([objNull, "VIEW"] checkVisibility [eyePos player,  _oldPos]<0.3)
					)
					then {
					_unit setPos _oldPos;
				};
				if (
						(_oldPos distance _playerPos>2) AND 
						((getPosATL _unit) distance _playerPos > _howClose)
					) then {
					//waitUntil {getPosATL _unit distance _currentDestination < 2};
					_currentDestination=getPosATL player;
					_unit doMove _currentDestination;
					_oldPos=getPosATL player;
				};
				if (_unit distance _currentDestination<_howClose*1.5) then { _unit forceWalk True}
					else { if (_unit distance _currentDestination>_howClose*3) then {_unit forceWalk False}
				}; 
			};
			default {
				unassignVehicle _unit;
				group _unit addVehicle (vehicle player);
				[_unit] orderGetIn true;
				waitUntil {sleep 1; vehicle player == player};
				[_unit] orderGetIn false;
				doGetOut _unit;
				_unit leaveVehicle (assignedVehicle _unit);
			};
		};	
	};
}


//Teleport on player when not looking with only TP/teleport
u=[r,3] spawn {
	params ["_unit",["_howClose", 3]];
	_oldPos=getPosASL player;
	_currentDestination=getPosASL player;
	//_unit doMove _oldPos;
	while {sleep 5; true} do {
		switch (vehicle player) do {
			case player : {
				_playerPos=getPosASL player;
				if (
						(_unit distance _playerPos > _howClose) AND
						([objNull, "VIEW"] checkVisibility [eyePos player, eyePos _unit]<0.3) AND
						([objNull, "VIEW"] checkVisibility [eyePos player,  _oldPos]<0.3) AND
						(speed _unit ==0) 
						//AND (FormationLeader _unit == player)
					)
					then {
					_unit setPosASL _oldPos;
				} else {
				  _oldPos=getPosASL player;
			      
				};
			};
			default {
				unassignVehicle _unit;
				group _unit addVehicle (vehicle player);
				[_unit] orderGetIn true;
				waitUntil {sleep 1; vehicle player == player};
				[_unit] orderGetIn false;
				doGetOut _unit;
				_unit leaveVehicle (assignedVehicle _unit);
			};
		};	
	};
}


//Alternative for TP/teleport follow
u=[r,3] spawn {
  params ["_unit",["_howClose", 3]];
  _oldPos=getPosASL player;
  while {sleep 1; true} do {
    _playerPos=getPosASL player;
    if (vehicle player != player) then {_unit moveInAny vehicle player};
    if (_unit distance _playerPos > _howClose) then {
    	_oldPos=getPosASL player;
    	waitUntil {sleep 1;
		([objNull, "VIEW"] checkVisibility [eyePos player, eyePos _unit]<0.3) AND
		([objNull, "VIEW"] checkVisibility [eyePos player,  _oldPos]<0.3) AND
		(speed _unit ==0) 
    	};
    	_unit setPosASL _oldPos;
    };
    
    _oldPos=getPosASL player;
  };
}

//lock UAV camera
http://killzonekid.com/arma-scripting-tutorials-uav-r2t-and-pip/

//targetting pod script
https://forums.bohemia.net/forums/topic/169324-release-script-tgp-targeting-pod-for-air-vehicles/

//spawn smoke grenade 
_smoke = "SmokeShellRed" createVehicle _somePosition;

//Ambient firefight
https://forums.bohemia.net/forums/topic/158822-ambient-combat-sound/

//Russians faces
Sergeant : Halliwell --Voice 1 -- Vitaly Vasilyev
Gren: Dayton -- Voice 2 -- Mikhail Khrushchev
MG: Collins -- Voice 3 -- Viktor Pushkin
Rifle : Markos -- Voice 5 -- Rashid Akhmedov

// CBA Clear building (my personal edit is below)
_group= group player;
_group = _group call CBA_fnc_getGroup;
if !(local _group) exitWith {}; // Don't create waypoints on each machine

private _building = nearestBuilding (leader _group);
if ((leader _group) distanceSqr _building > 250e3) exitwith {};

[_group, _building] spawn {
    params ["_group", "_building"];
    private _leader = leader _group;

    // Add a waypoint to regroup after the search
    _group lockWP true;
    private _wp = _group addWaypoint [getPos _leader, 0, currentWaypoint _group];
    private _cond = "({unitReady _x || !(alive _x)} count thisList) == count thisList";
    private _comp = format ["this setFormation '%1'; this setBehaviour '%2'; deleteWaypoint [group this, currentWaypoint (group this)];", formation _group, behaviour _leader];
    _wp setWaypointStatements [_cond, _comp];

    // Prepare group to search
    _group setBehaviour "Combat";
    _group setFormDir ([_leader, _building] call BIS_fnc_dirTo);

    // Leader will only wait outside if group larger than 2


    // Search while there are still available positions
    private _positions = _building buildingPos -1;
    while {!(_positions isEqualTo [])} do {
        // Update units in case of death
        private _units = (units _group) - [player];

        // Abort search if the group has no units left
        if (_units isEqualTo []) exitWith {};

        // Send all available units to the next available position
        {
            if (_positions isEqualTo []) exitWith {};
            if (unitReady _x) then {
                private _pos = _positions deleteAt 0;
                _x commandMove _pos;
                sleep 2;
            };
        } forEach _units;
    };
    _group lockWP false;
};

//Clear building my own edit
_group= group player; 
_group = _group call CBA_fnc_getGroup; 
if !(local _group) exitWith {}; // Don't create waypoints on each machine 
 
private _buildings = nearestObjects [(leader _group), ["House", "Building"], 300] ; 
if (count _buildings==0) exitwith {}; 
private _building = _buildings#0; 

 
[_group, _building] spawn { 
    params ["_group", "_building"]; 
    private _leader = leader _group; 
 
    // Add a waypoint to regroup after the search 
    _group lockWP true; 
    private _wp = _group addWaypoint [getPos _leader, 0, currentWaypoint _group]; 
    private _cond = "({unitReady _x || !(alive _x)} count thisList) == count thisList"; 
    private _comp = format ["this setFormation '%1'; this setBehaviour '%2'; deleteWaypoint [group this, currentWaypoint (group this)];", formation _group, behaviour _leader]; 
    _wp setWaypointStatements [_cond, _comp]; 
 
    // Prepare group to search 
    //_group setBehaviour "Combat"; 
	{_x setUnitPos "UP"} forEach units _group;
    _group setFormDir ([_leader, _building] call BIS_fnc_dirTo); 
 
    // Leader will only wait outside if group larger than 2 
 
 
    // Search while there are still available positions 
    private _positions = _building buildingPos -1; 
    while {sleep 0.1;!(_positions isEqualTo [])} do { 
        // Update units in case of death 
        private _units = (units _group) - [player]; 
 
        // Abort search if the group has no units left 
        if (_units isEqualTo []) exitWith {}; 
		
        // Send all available units to the next available position 
        { 
            if (_positions isEqualTo []) exitWith {};
			_oldPosArray=_x getVariable ["oldPosArray",[[0,0,0],0]];
			if (getPosATL _x distance _oldPosArray#0 < 1.5) then {
				_x setVariable ["oldPosArray",[_oldPosArray#0,1+_oldPosArray#1]];
			} else {
				_x setVariable ["oldPosArray",[getPosATL _x,0]];
			}
			_x setVariable ["oldPos",[getposATL _x,time];
            if (unitReady _x) then { 
                private _pos = _positions deleteAt 0; 
                _x commandMove _pos; 
                sleep 2; 
            }; 
        } forEach _units; 
    }; 
    _group lockWP false;
	{_x setUnitPos "AUTO"} forEach units _group;	
}; 



// change vest for all units
{	
	_myUnit=_x;
	if (side _myUnit == east) then {
		
		if (TRUE) then {
			_newVest="LOP_V_6B23_6Sh92_M81_OLV";
			_oldVestItems = VestItems _myUnit;
			_myUnit AddVest _newVest;
			{_myUnit addItemToVest _x;} forEach _oldVestItems;
		};
	};
} forEach allUnits;

// change gear/loadout and save it in 3den
//https://community.bistudio.com/wiki/save3DENInventory
save3DENInventory

//TAB targetting for gunner, ready to use:
moduleName_keyDownEHId = (findDisplay 46) displayAddEventHandler ["KeyDown",
{	
	params ["_displayorcontrol", "_key", "_shift", "_ctrl", "_alt"];
	//hint format["%1", _key];
	if (_key!=15) exitWith{};
	_unit=player;
	_gunner= units group player select 1;
	_targetsArray=_unit nearTargets 10000;
	_targetsArray=_targetsArray apply {[_x select 3, -1*((_x select 0) distance _unit),_x select 4]};
	_targetsArray sort false;
	_nextTarget=[0,'nothing',objNull];
	{
		if ((isNull assignedTarget _gunner) or (_x select 2!=assignedTarget _gunner)) 
			exitWith {_nextTarget=_x;};
	} foreach _targetsArray;

	if (_nextTarget select 0 < 10) then {_nextTarget=objNull} else {_nextTarget=_nextTarget select 2};
	
	_gunner reveal _nextTarget;
	(_gunner) doWatch _nextTarget;
	(_gunner) doTarget _nextTarget;
}];

//Moves infantry on scripted path
//Grabs the path ==> put objects in editor having the same name+number (_filter) and put them in same layer.
//Move infantry  unit on scripted path
TOV_fnc_grabPositions = {
	// filter is text, layer is text
	params [["_layer",""],["_filter",""],["_deleteAfterGrab",false]];
	if (_layer isEqualTo "") exitWith {systemchat "You didn't give a layer name !"};

	_wpMarkers = [];

	{
		if ((toUpper (str _x) find toUpper _filter >= 0) || _filter isEqualTo "")  then {_wpMarkers pushback _x};
	} forEach (getMissionLayerEntities _layer select 0);
	
	_wpMarkers=[_wpMarkers, [], {parseNumber ((str _x splitString "_") select 1)}, "ASCEND"] call BIS_fnc_sortBy;
	if (_deleteAfterGrab) then {{deleteVehicle  _x} forEach _wpMarkers;};
	_wpMarkers apply {getposATL _x};
};

//....................................................
//params: unit, array of pos, code to execute on end, animation, anim lenght
BIS_scriptedMoveEnabled=true;
Tova_fnc_moveOnPath= {
	
	params ["_guy","_wps",["_walkAnim","AmovPercMwlkSrasWrflDf"],["_animLength",1],["_code",{}]];

	_guy disableAI "MOVE";

	//calculate total distance to walk and how many times we need to play the walk animation
	_dist = _guy distance (_wps select 0);
	_i = 1;
	while {_i < count _wps} do {
		_dist = _dist + ((_wps select (_i - 1)) distance (_wps select _i));
		_i = _i + 1
	};
	_animLoops = ceil (_dist / _animLength);
	hint format ["SCRIPTED WALK: %1 will walk %2 meters using %3 animations '%4'.", name _guy, _dist, _animLoops, _walkAnim];
	
	[_guy, _wps] spawn {
		_guy = _this select 0;
		_wps = _this select 1;
		_i = 0;
		
		//procedure for smooth turning
		_turningProc = {
			_this spawn {
				_guy = _this select 0;
				_wp = _this select 1;
				_dir = [_guy, _wp] call BIS_fnc_dirTo;
				if (_dir < 0) then {_dir = 360 + _dir};
				_degs = _dir - direction _guy;
				if (abs _degs > 180) then {_degs = _degs + (360 * (_degs / abs _degs) * (-1))};
				_step = _degs / 20;
				while {(abs _degs > abs _step) && BIS_scriptedMoveEnabled} do {
					_guy setDir (direction _guy + _step);
					_degs = _dir - direction _guy;
					sleep 0.025
				};
				_guy setDir ([_guy, _wp] call BIS_fnc_dirTo)
			}
		};
		
		//make sure the unit faces its first waypoint
		[_guy, _wps select 0] call _turningProc;
		
		//turn the unit at each waypoint
		while {_i < count _wps-1} do {
			//hint format ["%1",_i];
			waitUntil {(getposATL _guy) distance (_wps select _i) < 0.5 || if (_i > 0) then {(_guy distance (_wps select (_i - 1))) > ((_wps select _i) distance (_wps select (_i - 1)))} else {FALSE}};
			[_guy, _wps select (_i + 1)] call _turningProc;
			hint format ["SCRIPTED WALK: %1 just reached WP #%2 (of %3).", name _guy, _i + 1, count _wps];
			_i = _i + 1;
		}
	};
	
	waitUntil {[_guy, _wps select 0] call BIS_fnc_relativeDirTo < 30};
	
	//playing walk animations
	_i = 1;
	while {(_i <= _animLoops && !(isPlayer _guy)) && BIS_scriptedMoveEnabled} do {
		//_nic = [objNull, _guy, rPLAYMOVE, _walkAnim] call RE;
		_guy playMove _walkAnim;
		_i = _i + 1;
		sleep 0.1
	};
	
	//a code can be executed once the unit finished walking
	waitUntil {_guy distance (_wps select (count _wps -1)) < 0.5};
	_guy enableAI "MOVE";
	_guy switchMove "";
	[_guy] call _code
}

cat=["wpVilla","wpp"] call GOM_fnc_grabPositions;
[r,cat,{},"AmovPknlMwlkSlowWrflDf",2.5] spawn Tova_fnc_moveOnPath


//Create eden arrows
fnc_Create_Arrows_In_Eden =
{
	params["_posArray",["_objName","obj"],["_layerName",""]];
	_num = 0;
	if (_layerName=="") then {_layerName=_objName+"Layer"};
	_myLayer = -1 add3DENLayer _layerName;
	{
		_id = _objName +"_" + str _num;
		_obj = create3DENEntity ["Object", "VR_3DSelector_01_incomplete_F", [0,0,0]];
		_obj set3DENAttribute ["Position", _x];
		_obj set3DENAttribute ["Name", _id];
		_obj set3DENLayer _myLayer;

		_num = _num + 10;
	} forEach _posArray;
};

//calculate path pathArray finder
pathArray=[];
markersNb=0;
(calculatePath ["car","safe",getMarkerPos "w_1",getMarkerPos "w_2"]) addEventHandler ["PathCalculated",{
	{
		_mrk = createMarker ["marker" + str (markersNb+_forEachIndex), _x];
		_mrk setMarkerType "mil_dot";
		//_mrk setMarkerText str _forEachIndex;
		pathArray=pathArray+[_x]
	} forEach (_this#1);
}]


//calculate path pathArray finder for all group waypoints
TOV_drawPath= {
	params ["_myGroup"];
	pathArray=[];
	_allWaypoints=waypoints _myGroup;
	for "_i" from 0 to ((count _allWaypoints)-2) do
	{
		TOV_drawPathI=_i;
		(calculatePath ["car","safe",getWPPos (_allWaypoints#_i),getWPPos (_allWaypoints#(_i+1))]) addEventHandler ["PathCalculated",{
			{
				_mrk = createMarker ["marker" + (str TOV_drawPathI) + (str time)+str random(5000), _x];
				_mrk setMarkerType "mil_dot";
				//_mrk setMarkerText str _forEachIndex;
				pathArray=pathArray+[_x]
			} forEach (_this#1);
		}];
	};
};
[group player] spawn TOV_drawPath;

//Move and stay, to move subSquads somewhere, used for scripted bounding, myUnits must be an array
moveAndStay= {
	params ["_myUnits","_destination",["_finalBehaviour","AWARE"],["_finalStance","AUTO"]];
	_myUnits=_myUnits-[player];
	_safePosArray=[_destination, 0, 2, 0, 0];
	_initialGroup=group (_myUnits select 0);
	_tmpGroup=createGroup [side (_myUnits select 0), false];  
	{
		_x disableAI "AUTOCOMBAT";
		_x setUnitPos "AUTO";
		[_x] joinSilent _tmpGroup;
		_x setBehaviour "AWARE";
		[_x] joinSilent _initialGroup;
		_x doMove (_safePosArray call BIS_fnc_findSafePos);
	} forEach _myUnits;
	
	sleep 1;
	_stillMovingUnits=_myUnits;
	while {sleep 0.5; count _stillMovingUnits>0 } do {
		hintSilent str _stillMovingUnits;
		{
			if (unitReady _x) then {
				doStop _x;
				[_x] joinSilent _tmpGroup;
				_x setUnitPos _finalStance;
				_x setBehaviour _finalBehaviour;
				[_x] joinSilent _initialGroup;
				_x enableAI "AUTOCOMBAT";
				_stillMovingUnits=_stillMovingUnits-[_x];
				doStop _x;
				};
		} forEach _stillMovingUnits;
	};
	deleteGroup _tmpGroup;
	hint "over";
};

//No AI Prone
TOV_noProne= {
	params ["_unit"];
	if !(isServer) exitWith {false};
	
	_unit setVariable ["TOV_noProne",true,true];
	while {alive _unit && _unit getVariable "TOV_noProne"} do {
		if (behaviour _unit =="COMBAT") then {
			waitUntil {sleep 1; speed _unit < 1};
			_unit setUnitpos "MIDDLE";
			waitUntil {sleep 1; speed _unit > 2};
			_unit setUnitpos "UP";
		} else {
			_unit setUnitpos "AUTO";
			sleep 2;
		};
	};
	_unit setUnitpos "AUTO";
	_unit setVariable ["TOV_noProne",false,true];
};



{
		[_x] spawn TOV_noProne;
} forEach (units group player)-[player];




////Firing Drills with custom weapons

/*
Female bit : 
player setFace "TCGM_Fem_Fox";
player setSpeaker "cup_d_female01_en";
*/
[] spawn {
	resetLoadout=
	
	[["CUP_arifle_AK74M","","","CUP_optic_1p63",["CUP_30Rnd_545x39_AK74M_M",30],[],""],[],["hgun_Rook40_F","","","",["16Rnd_9x21_Mag",17],[],""],["U_Competitor",[]],["CUP_V_I_RACS_Carrier_Rig_3",[["16Rnd_9x21_Mag",2,17],["CUP_30Rnd_545x39_AK74M_M",2,30]]],[],"H_Cap_headphones","CUP_G_Oakleys_Clr",["Binocular","","","",[],[],""],["ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""]]
	
	;newLoadout=true;
	player setUnitLoadout resetLoadout;
	_resetAllowedGuns={
		{
			_currentCP=_x;
			_allowedGuns=_currentCP getVariable ["weapons", []];
			if !(_allowedGuns isEqualTo []) then {
				if (_allowedGuns#0 isKindOf ["Rifle", configFile >> "CfgWeapons"]) then {
					_currentCP setVariable ["weapons",[resetLoadout#0#0]];
				} else {
					_currentCP setVariable ["weapons",[resetLoadout#2#0]];
				};
			};
		} forEach BIS_FD_CPs;
	};
	call _resetAllowedGuns;
	
	while {newLoadout} do {
		waitUntil {sleep 0.1;(isNil "RscFiringDrillTime_done" || RscFiringDrillTime_done) &&
			(!((currentWeapon player) in resetLoadout#0)
			&& !((currentWeapon player) in resetLoadout#2))
			};
		player setUnitLoadout resetLoadout;
		call _resetAllowedGuns;
		waitUntil {sleep 1;!RscFiringDrillTime_done};
	};
};


//ACE AI drivers Limit speed
currentMaxSpeed = "No limit";
private _self_maxSpeed = ['maxSpeed','Limit speed to ','',{hintSilent format["Current max speed: %1", currentMaxSpeed];},{vehicle player != player}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions"], _self_maxSpeed] call ace_interact_menu_fnc_addActionToObject;

private _self_speedValue = ['noLimit','Limitless','',{currentMaxSpeed = "No limit"; (vehicle player) limitSpeed -1;},{!("No limit" isEqualTo currentMaxSpeed)}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions", "maxSpeed"], _self_speedValue] call ace_interact_menu_fnc_addActionToObject;

{
	private _self_speedValue = [format["view%1",_x],format["%1",_x],'',compile format["currentMaxSpeed=%1;(vehicle player) limitSpeed %1;", _x],compile format["!(%1 isEqualTo currentMaxSpeed)",_x]] call ace_interact_menu_fnc_createAction;
	[player, 1, ["ACE_SelfActions", "maxSpeed"], _self_speedValue] call ace_interact_menu_fnc_addActionToObject;
} forEach [30,50,70,90,120];

//Scale ACE damage for a unit
a setVariable ["ace_medical_DamageThreshold",100]

//Open ACE arsenal
[player, player, true] call ace_arsenal_fnc_openBox
[this, true] call ace_arsenal_fnc_initBox

//Enable fortify
[player, [], true] call ace_fortify_fnc_setupModule

//getmortar elevation
[vehicle player, [0]] call CBA_fnc_turretDir
//get angle in podnos
onEachFrame {
	_vehicle=vehicle player; 
	_typeOf = typeOf _vehicle; 
	_turret=[0]; 
	_turretCfg = [_typeOf, _turret] call CBA_fnc_getTurret; 
	_turretAnimBody = getText (_turretCfg >> "animationSourceBody"); 
	 
	_weaponDir = _vehicle weaponDirection (currentWeapon _vehicle); 
	_currentTraverseRad = _vehicle animationSourcePhase _turretAnimBody; 
	if (isNil "_currentTraverseRad") then { _currentTraverseRad = _vehicle animationPhase _turretAnimBody; }; 
	 
	_turretRot = [vectorDir _vehicle, vectorUp _vehicle, deg _currentTraverseRad] call CBA_fnc_vectRotate3D; 
	_realElevation = (acos ((_turretRot vectorCos _weaponDir) min 1)) + ((_turretRot call CBA_fnc_vect2polar) select 2); 
	if (_realElevation > 90) then { _realElevation = 180 - _realElevation; }; 
	_realElevation=((round (_realElevation * 6400 / 360)) % 6400);
	
	([vehicle player, [0]] call CBA_fnc_turretDir) params ["_direction","_angle"];
	
	hintSilent format ["Azimuth : %1°\nElevation : %2 mil\nCharge : %3",round(_direction),_realElevation,currentWeaponMode player]
}

//ACE AI pilots flyInHeight
currentAltitude = 100;
private _self_altitude = ['altitude','Set altitude to','',{hintSilent format["Current requested altitude : %1", currentAltitude];},{vehicle player != player}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions"], _self_altitude] call ace_interact_menu_fnc_addActionToObject;

{
	private _self_altitudeValue = [format["view%1",_x],format["%1",_x],'',compile format["currentAltitude=%1;(vehicle player) flyInHeight %1;", _x],compile format["!(%1 isEqualTo currentAltitude)",_x]] call ace_interact_menu_fnc_createAction;
	[player, 1, ["ACE_SelfActions", "altitude"], _self_altitudeValue] call ace_interact_menu_fnc_addActionToObject;
} forEach [20,50,70,100,150,200,300,400,500,600,700,800,900,1000];

//Survivable crash landings
[] spawn {
	_veh=vehicle player ;
	_veh setDamage 0.9;
	waitUntil {(getposATL _veh)#2<10}; 
	_veh allowDamage false; 
	waitUntil {sleep 0.01;( vectorMagnitude velocity _veh) < 1}; 
	_veh allowDamage true;
}

//convert modern MSV to desert MSV
convertToDesert={
	_myUnit=_this;
	
	/*_desertUniforms=[
		"CUP_U_O_RUS_BeigeDigital_VDV",0.7,
		"CUP_U_O_RUS_BeigeDigital_VDV_gloves_pads",1,
		"CUP_U_O_RUS_BeigeDigital_VDV_rolled_up",0.5,
		"CUP_U_O_RUS_BeigeDigital_VDV_rolled_up_gloves_pads",0.6
	];*/
	
	_desertUniforms=[
		"CUP_U_O_RUS_BeigeDigital_MSV",0.7,
		"CUP_U_O_RUS_BeigeDigital_MSV_gloves_pads",1,
		"CUP_U_O_RUS_BeigeDigital_MSV_rolled_up",0.5,
		"CUP_U_O_RUS_BeigeDigital_MSV_rolled_up_gloves_pads",0.6
	];

	_desertVests=[
		"CUP_V_RUS_6B45_1_BeigeDigital",0.5,
		"CUP_V_RUS_6B45_2_BeigeDigital",0.5,
		"CUP_V_RUS_6B45_3_BeigeDigital",1
	];

	_desertHelmets=[
		"CUP_H_RUS_6B27_cover_BeigeDigital",
		"CUP_H_RUS_6B27_cover_BeigeDigital_goggles",
		"CUP_H_RUS_6B27_cover_BeigeDigital_headset_goggles",
		"CUP_H_RUS_6B27_cover_BeigeDigital_headset"
	];

	_desertBackpacks=[
		"B_Kitbag_tan"
	];

	_properOptics=[
		"cup_optic_1p63",1,
		"cup_optic_pechenegscope",0.5
	];

	//Give new uniform
	_newGear=selectRandomWeighted _desertUniforms;
	_oldItems = uniformItems _myUnit;
	_myUnit forceAddUniform _newGear;
	{_myUnit addItemToUniform _x;} forEach _oldItems;

	//Give new vest
	_newGear=selectRandomWeighted _desertVests;
	_oldItems = vestItems _myUnit;
	_myUnit addVest _newGear;
	{_myUnit addItemToVest _x;} forEach _oldItems;

	//Give new Helmet
	_newGear=selectRandom _desertHelmets;
	_myUnit addHeadgear _newGear;

	//replace backpack
	if ("CUP_B_RUS_Pack" in backpack _myUnit) then {
		_oldItems = backpackItems _myUnit;
		_newGear = selectRandom _desertBackpacks;
		removeBackpack _myUnit;
		_myUnit addBackpack _newGear;
		{_myUnit addItemToBackpack _x;} forEach _oldItems;
	};

	//replace optics
	_opticsToReplace=["CUP_optic_ekp_8_02","CUP_optic_Kobra"];
	if (count (_opticsToReplace arrayIntersect (primaryWeaponItems _myUnit))>0) then {
		_newGear = selectRandomWeighted _properOptics;
		{_myUnit removePrimaryWeaponItem _x;} forEach _opticsToReplace;
		_myUnit addPrimaryWeaponItem  _newGear;
	};
	
	//Add default gear
	//_myUnit addPrimaryWeaponItem "CUP_acc_LLM01_L";
	_myUnit addItemToVest "CUP_NVG_HMNVS";
	
	save3DENInventory [_myUnit];
};

{_x  spawn convertToDesert} forEach (get3DENSelected "object");

//remove female faces un-womanize
_femaleFaces=["Mason",
"Mason_h",
"Oakes",
"Oakes_h",
"Smith",
"Smith_h"];
{
	if ((face _x) in _femaleFaces) then {
		_nb=(round (1+random 27));
		_newFace="";
		if (_nb<10) then { _newFace="WhiteHead_0"+ (str _nb)} else {_newFace="WhiteHead_"+ (str _nb)};
		_x set3DENAttribute ["face",_newFace];
		_x setFace _newFace;
	};
} forEach (get3DENSelected "object");


//remove female faces un-womanize V2
{
	if (["female", face _x] call BIS_fnc_inString) then {
		_nb=(round (1+random 27));
		_newFace="";
		if (_nb<10) then { _newFace="WhiteHead_0"+ (str _nb)} else {_newFace="WhiteHead_"+ (str _nb)};
		_x set3DENAttribute ["face",_newFace];
		_x setFace _newFace;
	};
} forEach (get3DENSelected "object");

//superHot mode
TOV_superHot= {
	params ["_unit"];
	if !(isServer) exitWith {false};
	
	_unit setVariable ["TOV_superHot",true,true];
	while {alive _unit && _unit getVariable "TOV_superHot"} do {
		waitUntil {Uisleep 0.1; vectorMagnitude velocity _unit < 1};
		setAccTime 0.4;
		waitUntil {Uisleep 0.1; vectorMagnitude velocity _unit > 2};
		setAccTime 1;

	};
	setAccTime 1;
	_unit setVariable ["TOV_superHot",false,true];
};
[player] spawn TOV_superHot;

//Karl AI mini-menu
	groupSelectedUnits player;
	
// Crouch if suppressed
TOV_crouchWhenSuppressed= {
	params ["_unit"];
	if !(isServer) exitWith {false};
	
	_unit setVariable ["TOV_crouchWhenSuppressed",true,true];
	while {alive _unit && _unit getVariable "TOV_crouchWhenSuppressed"} do {
		if (getSuppression  _unit >0.7) then {
			_unit setUnitpos "MIDDLE";
			waitUntil {sleep 2; getSuppression  _unit <0.4};
			_unit setUnitpos "AUTO";
		} else {
			_unit setUnitpos "AUTO";
			sleep 2;
		};
	};
	_unit setUnitpos "AUTO";
	_unit setVariable ["TOV_crouchWhenSuppressed",false,true];
};

TOV_crouchWhenSuppressed= {
	params ["_unit"];
	if !(isServer) exitWith {false};
	
	_unit setVariable ["TOV_crouchWhenSuppressed",true,true];
	while {alive _unit && _unit getVariable "TOV_crouchWhenSuppressed"} do {
		if (getSuppression  _unit >0.7) then {
			_unit setUnitpos "MIDDLE";
			waitUntil {sleep 2; getSuppression  _unit <0.4};
			_unit setUnitpos "AUTO";
		} else {
			_unit setUnitpos "AUTO";
			sleep 2;
		};
	};
	_unit setUnitpos "AUTO";
	_unit setVariable ["TOV_crouchWhenSuppressed",false,true];
};


{
		_x setVariable ["TOV_crouchWhenSuppressed",false,true];
} forEach (units group player)-[player];

{
		[_x] spawn TOV_crouchWhenSuppressed;
} forEach (units group player)-[player];


//Unstick an AI unit
_unstickUnit = 
{
	params ["_unit"];
	_newPos=[_unit, 0, 2, 1] call BIS_fnc_findSafePos;
	_unit setPos _newPos;
	//_unit setVehiclePosition [_pos vectorAdd [0,0,_add], [], 0, "NONE"];
	_unit doWatch objNull;
	//_unit selectWeapon (primaryWeapon _unit);
	_unit switchMove "";
	_unit setVelocity [0,0,0];
};


//Liberation cheats
// Crate with 100 supplies spawn at player position.
[] call KPLIB_fnc_createCrate;
// Crate with 100 ammo spawn at player position.
[KP_liberation_ammo_crate] call KPLIB_fnc_createCrate;
// Crate with 100 fuel spawn at player position.
[KP_liberation_fuel_crate] call KPLIB_fnc_createCrate;
//Change reputation with civilians +amount
[amount] spawn F_cr_changeCR;

//make a FOB vehicle
ss=createVehicle ["CUP_O_BTR90_HQ_RU",getpos player];
[ss] call KPLIB_fnc_addActionsFob;

//make items nearby carryable
//To find the type of object category, aim at it and use : [configFile >> "CfgVehicles" >> typeOf cursorObject, true ] call BIS_fnc_returnParents;
TOV_make_carryable= {
  params [["_loop_it", False],["_change_owner",True]];
  while {True} do {
    {
      if (_change_owner) then {[_x,  clientOwner] remoteExec ["setOwner", [2,_x]];};
      [_x, true] remoteExec ["ace_dragging_fnc_setCarryable",0] ;
      [_x, 0.1] remoteExec ["ace_cargo_fnc_setSize",0] ;
    } forEach nearestObjects [player, ["Thing","NonStrategic","FloatingStructure_F"], 5];
    if !(_loop_it) exitWith {True};
    sleep 10;
  };
};
carry_handle = [] spawn TOV_make_carryable;

//Carryable from zeus
{ [_x,  clientOwner] remoteExec ["setOwner", [2,_x]];;
  [_x, true] remoteExec ["ace_dragging_fnc_setCarryable",0] ;
  [_x, 0.1] remoteExec ["ace_cargo_fnc_setSize",0] ;
} forEach nearestObjects [_this, ["Thing","NonStrategic","FloatingStructure_F"], 5];

//Set vehicle cargo space
[cursorObject, 20] remoteExec ["ace_cargo_fnc_setSpace",0] ;



//Looped


//delete crates nearby
{
  deleteVehicle _x;
} forEach nearestObjects [player, [KP_liberation_fuel_crate], 5];


//reorder group player
[] spawn {
	_oldGroup=group player; 
	[player] joinSilent grpNull;
	sleep 1; 
	{[_x] joinSilent group player} forEach units _oldGroup;
}

//map Liberation replacer on grid
//publicVariable "TOV_fnc_TohToCupReplacement";
[("183221" call BIS_fnc_gridToPos)#0
	,3000] spawn TOV_fnc_TohToCupReplacement;

//Forward observer AI
TOV_ForwardObserverAI={
	// handle=[[mortar],FO] spawn TOV_ForwardObserverAI
	params["_listOfArtillery","_forwardObserver",["_range",2000],["_burstSize",3]];
	
	_forwardObserver setSkill ["spotDistance", 1];
	_forwardObserver setSkill ["spotTime", 1];
	//_forwardObserver selectWeapon (assignedItems _forwardObserver select 4);
	_positionsToShootAt=[];
	
	while {
		(_forwardObserver getVariable ["TOV_allowCallArtillery",True]) &&
		(alive _forwardObserver) &&
		!(_listOfArtillery isEqualTo [])
	} do {
		//updating list of available guns (alive and has ammo)
		_listOfArtillery=_listOfArtillery select { (alive _x) && (someAmmo _x)};
		
		//If no targets, ask the FO is he knows any
		if (_positionsToShootAt isEqualTo []) then {
			_knownBadguys= _forwardObserver nearTargets _range select {(_x select 3)>1};
			_knownBadguys=_knownBadguys apply {[_x select 3, _x select 0]};
			_knownBadguys sort false;
			
			{
				_currentBadGuyPosition=_x#1;
				if ( ({ (_currentBadGuyPosition distance _x < _range) && ((getpos _forwardObserver) distance _x < _range) } count _positionsToShootAt) == 0 ) then {
					_positionsToShootAt=_positionsToShootAt + [_currentBadGuyPosition];
				};
			} forEach _knownBadguys;
		};
		
		//Shooting proper
		{
			if (_positionsToShootAt isEqualTo []) exitWith {};
			if (unitReady _x) then {
				_currentTarget = _positionsToShootAt deleteAt 0; 
				_x commandArtilleryFire [
					//_currentTarget,
					[[[_currentTarget, 10]], []] call BIS_fnc_randomPos,
					getArtilleryAmmo [_x] select 0, 
					_burstSize 
				   ];
			};
		} forEach _listOfArtillery;
		
		
		sleep 30;
	};
	_forwardObserver;
};

//To reveal all baddies in front of forwardObserver
_forwardObserver=ss;
TOV_revealToFO={
	params ["_forwardObserver"];
	{_forwardObserver reveal vehicle _x} forEach (allunits select {
								(_x distance2D _forwardObserver < 1700) &&
								([getpos _forwardObserver, getdir _forwardObserver, 160, getpos _x] call BIS_fnc_inAngleSector) 
								});
};

//To reveal units known to FAC
TOV_revealFromFAC={
	params ["_forwardObserver","_casTeam",["_range",2000]];
	_knownUnits=_forwardObserver nearTargets _range;
	{
		_casTeam reveal [_x#4, 4 min 1.5*(_forwardObserver knowsAbout _x#4)];
	} forEach _knownUnits;
};

handle=[] spawn {while {true} do { [ss,cas] call TOV_revealFromFAC; sleep 30;}};


//Make player non leader of group
{
	if (!isPlayer _x) exitWith {group player selectLeader _x;}
} forEach units group player;

//Liberation show radio/chat/conversation if ACE arsenal is opened by script
showChat true;

//add groups to HC
 { 
  if (player != hcLeader _x) then
    {
      hcLeader _x hcRemoveGroup _x;
      player hcSetGroup [_x];
    }
  } foreach groups blufor;
  
  
//reset groups
{ 
_group = createGroup [west, true];
(units _x) joinSilent _group;
} foreach groups blufor;


//set date/time to today + speed =1
GRLIB_Time_factor=1;
setDate [2024, 6, 28, 19, 23]; // 4:00pm February 25, 1986


//Reduce distance to next village for spawning a base
GRLIB_fob_range=0;
//GRLIB_capture_size=XXX

//Stop moving AI on disconnect liberation
me=player;
{ 
  {_x disableAI "MOVE" } forEach (units group me) ;
  } remoteExec ["call", 0, True];
  
//Alternative
{
 [_x, "MOVE"] remoteExec ["disableAI", 0, True];
 } forEach (units group player);
 
 {
   [_x, "PATH"] remoteExec ["disableAI", 0, True];
   [_x, "BLUE"] remoteExec ["setCombatMode", 0, True];
 } forEach (units group player);

//infinite fuel
refuel_handle=[] spawn {while {True} do {{vehicle _x setfuel 1} forEach (units group player);{_x setFuel 1} forEach allUnitsUAV; sleep 60;}}

//Liberation show radio/chat/conversation if ACE arsenal is opened by script
showChat true;

//more civvies (by def from 0 to 2)
GRLIB_civilian_activity=10;

//Make mission harder (more units spawned, value between 1 and 10 by default, AGGRESSIVITY between 1 and )
GRLIB_difficulty_modifier=10;
GRLIB_csat_aggressivity=4;

//Smarter lambai with reinforcement
smart_handle= [] spawn {
  while {true} do {
    {
      _myGroup=_x;
      if (side _myGroup == west) then { continue };
      {_x setVariable ["lambs_danger_dangerRadio", true, owner _x];} forEach (units _myGroup);
      _myGroup setVariable ["lambs_danger_enableGroupReinforce", true, owner _myGroup];
      sleep 1;
    } forEach allGroups;
    sleep 60;
  };
};

//Override TP to everywhere
TOV_a_TP={
	params["_target","_pos","_is3D","_unitsToTeleport"];
	//hint format ["target %1", _target];
	
	//if (!_is3D || !(_target isKindOf "Building")) exitwith {False};
	
	private _distance = viewDistance;
        private _origin = AGLToASL positionCameraToWorld [0, 0, 0];
        private _target = AGLToASL positionCameraToWorld [0, 0, _distance];

        private _default = _origin vectorAdd (_origin vectorFromTo _target vectorMultiply _distance);
        _target = lineIntersectsSurfaces [_origin, _target, cameraOn] param [0, [_default]] select 0;
	
	_unitsToTeleport = _unitsToTeleport select {(_x distance2D _target <TOV_AI_allowTeleports_range) && (vehicle _x==_x)}; 
	
	{
		_x setPosASL _target; //NOTE THE ASL
		dostop _x;
	} forEach _unitsToTeleport;
};

//get position from marker name
_markerName="mf";
_myMarkerPos=getMarkerPos ((allMapMarkers select { markerText _x  == _markerName})#0);

//Create minefield 
_mapPosition = getPos player; 
_radius=100; 
_howMany=50; 
 
for "_i" from 1 to _howMany do {  
  _randomPos = [[[_mapPosition, _radius]], []] call BIS_fnc_randomPos; 
  createMine [ 
    selectRandom ["ATMine", "APERSMine", "IEDLandSmall_F", "IEDLandBig_F"], 
    _randomPos, [], 5]; 
}

//making hole
_pos= getposASL player;
setTerrainHeight [[[1005, 1000, 50]]];

//Spawn group and have it go somewhere (for ambush)
_wp_grid_pos = ["06350615","07610763","09190718"];

_spawn_point=(_wp_grid_pos#0 call BIS_fnc_gridToPos)#0;
_wp_grid_pos deleteAt 0;

baddies=[_spawn_point, EAST,
(configFile >> "CfgGroups" >> "East" >> "CUP_O_TK" >> "Armored" >> "CUP_O_TK_T34Platoon"),
[],[],[],[],[],180,False
] call BIS_fnc_spawnGroup;

{
  _destination_x = (_x call BIS_fnc_gridToPos)#0;
  _wp = baddies addWaypoint [_destination_x, 0];
  _wp setWaypointCombatMode "RED";
  _wp setWaypointBehaviour "SAFE";
  _wp setWaypointSpeed "LIMITED";
  _wp setWaypointFormation "COLUMN";
} forEach _wp_grid_pos;

_final_target=(_wp_grid_pos#-1 call BIS_fnc_gridToPos)#0;
//[baddies, _final_target, 400, false] call CBA_fnc_taskAttack;
//[baddies, _final_target, 200, 3, 0.1, 0.5] call CBA_fnc_taskDefend;
baddies deleteGroupWhenEmpty true;


///////////////Grab and spawn
//Grab
[(getPos player) select [0,2], 20, true] call BIS_fnc_objectsGrabber;


//Spawn
//before running replace "" with "
_objectsArray=/*Copy paste here*/
;
_objectsArray = _objectsArray select { !(_x#0 isKindOf "House") };

[[0,0], 0, _objectsArray, 0] call BIS_fnc_objectsMapper;

//select al blufor group with more than 3 units
allGroups select { (side _x  == side player) and (count units _x >=3)}

//Animation car free
player switchmove "CUP_AN2_Cargo01"

//Helicopters spotting
terminate TOV_heliSpotting;
TOV_heliSpotting=[] spawn {
  while {True} do {
    {
      _currentAirUnit=_x;
      {
        _currentPlayer=_x;
        _currentKnowledge = EAST knowsAbout _currentPlayer;
        [_currentAirUnit, [_currentPlayer, _currentKnowledge]] remoteExec ["reveal", _currentAirUnit];
        _currentAirUnit reveal [_currentPlayer, _currentKnowledge]; 
      } forEach allUnits;
    } forEach allUnits select {_x isKindOf "AIR"};
    sleep (90 + random 300);
  };
};

// food and water
//Liberation loop
TPV_FnW= {
  //setup on respawn
  player addEventHandler ["Respawn", {
    params ["_unit", "_corpse"];
    //missionProfileNamespace
    _water=profileNamespace getVariable ["acex_field_rations_thirst", 0];
    _food=profileNamespace getVariable ["acex_field_rations_hunger", 0];

    _water = _water min 90;
    _food = _food min 90;

    player setVariable ["acex_field_rations_thirst", _water];
    player setVariable ["acex_field_rations_hunger", _food];
  }];

  _water=profileNamespace getVariable ["acex_field_rations_thirst", 0];
  _food=profileNamespace getVariable ["acex_field_rations_hunger", 0];
  player setVariable ["acex_field_rations_thirst", _water];
  player setVariable ["acex_field_rations_hunger", _food];

  while {sleep 60; True} do {
    if !(alive player) then {continue};
    _water=player getVariable "acex_field_rations_thirst";
    _food=player getVariable "acex_field_rations_hunger";
    
    _water = _water min 90;
    _food = _food min 90;
    
    profileNamespace setVariable ["acex_field_rations_hunger", _food];
    profileNamespace setVariable ["acex_field_rations_thirst", _water];
    saveProfileNamespace;
  };
};
[] spawn TPV_FnW;
 

player setVariable ["acex_field_rations_hunger", 50];
player setVariable ["acex_field_rations_thirst", 50];

ace_leg_face

player getVariable "acex_field_rations_thirst"
player getVariable "acex_field_rations_hunger"

//Jerrycans
can=createvehicle ["Land_CanisterFuel_F",getpos player];
[can] remoteExec ["ace_refuel_fnc_makeJerryCan",0];
[can, true] remoteExec ["ace_dragging_fnc_setCarryable",0];
can

//make throwable pickable
_action = ["pickUp_to_throw","Pick up","",{
        _target setVariable ["ace_advanced_throwing_throwable",_target];
        [_target, _player] call ace_advanced_throwing_fnc_pickUp;
    },{true},{},[_target, _player], [0,0,0], 100] call ace_interact_menu_fnc_createAction;

[cursorTarget, 0, ["ACE_MainActions"], _action]  remoteExec ["ace_interact_menu_fnc_addActionToObject",0];

//TOV radio everywhere
TOV_radio_everywhere=True;

//Add everything to zeus
_addons=[];
_cfgPatches = configFile >> "cfgPatches";

for "_i" from 0 to (count _cfgPatches - 1) do {
 _class = _cfgPatches select _i;
 if (isClass _class) then { 
  _addons pushBack (configName _class);
 };
};
_addons;

{_x addCuratorAddons _addons } forEach allCurators 


//Disable liberation fuel management
kp_fuel_consumption_vehicles=compileFinal "[]";

//Add ace injuries
pat1=cursorObject;
[pat1] call ace_medical_fnc_handleDamage_advancedSetDamage;
pat1 setUnconscious false;
pat1 setVariable ["ace_medical_ai_lastFired", 999999];

_possible_places=["head","body","hand_l","hand_r","leg_l","leg_r"];
_injuries_types= ["bullet", "grenade", "explosive", "shell", "vehiclecrash", "backblast", "stab", "punch", "falling", "ropeburn"];

for "_i" from 0 to (random 10) do {
    [pat1, 0.1+random 0.3, 
    selectrandom _possible_places, 
    selectrandom _injuries_types
    ] call ace_medical_fnc_addDamageToUnit;
};

//multiplayer spotted easilier
{
  _x setUnitTrait ["audibleCoef", 4];
  _x setUnitTrait ["camouflageCoef", 10];
} forEach (units group player)


//Making woman Tanya
_u=_this;
[_u, "FaceW01"] remoteExec ["setFace", 0, True];
[_u, "cup_d_female01_en"] remoteExec ["setSpeaker", 0, True];
[_u, "Tanya Starygina", "Tanya", "Starygina"] remoteExec ["setName", 0, True];

//clean server spam
deleteVehicle objectFromNetId "2:738";


//Arma AI talking
//magic variable to enable/disable
TOV_AI_SPEAKING=True;
publicVariable "TOV_AI_SPEAKING";
//Use player custom identity with :
player setVariable ["TOV_custom_name", "Tony", True];



//Yulakia liberation setup unit
_dude=_this
_uni = "CUP_U_O_RUS_Soldier_Reversible_Suit_6Sh122_Autumn_2" + selectRandom ["1", "2", "3", "4"];
_dude forceaddUniform _uni;
removeGoggles  _dude;

//update build list liberation
KPLIB_buildList = [[], infantry_units, light_vehicles, heavy_vehicles, air_vehicles, static_vehicles, buildings, support_vehicles, KPLIB_b_allSquads];

//Increase max squad size
infantry_cap=100;
GRLIB_max_squad_size=100;
GRLIB_blufor_cap=100;

//Export mission :
 /* 0: Position <ARRAY> (default: [0, 0, 0])
 * 1: Radius <NUMBER> (default: -1)
 *   - in meters, -1 for entire mission
 * 2: Include Waypoints <BOOL> (default: true)
 * 3: Include Markers <BOOL> (default: false)
 * 4: Curator Editable Objects Only <BOOL> (default: false)
 */
[screenToWorld [0.5, 0.5], 200, true, false, false] call zen_common_fnc_exportMissionSQF


//Code to become zeus
TOV_elevate_zeus = {
  params ["_unit"];
   
  private _moduleGroup = createGroup sideLogic;

  _zeus = _moduleGroup createUnit ["ModuleCurator_F", getPosATL _unit, [], 0, "NONE"];
  
  _zeus setVariable ["owner", _unit];
  
  _unit assignCurator _zeus;
};

zeus_handle = addMissionEventHandler ["HandleChatMessage", {
    params ["_channel", "_owner", "_from", "_text", "_person", "_name", "_strID", "_forcedDisplay", "_isPlayerMessage", "_sentenceType", "_chatMessageType", "_params"];
    
    if (_text == "*zeus") then {
      [_person] call TOV_elevate_zeus;
    };
    
    [_from, _text] ;
}];



TOV_caching={
    //[_newGroup, _garrison, _playersDistance] spawn TOV_caching;
    //{[_x, true] spawn TOV_caching} forEach (allGroups select {side _x == EAST});
    params ["_originalGroup", ["_garrison", True], ["_playersDistance", 2000]];
    
    if (_originalGroup getVariable ["TOV_caching_running", False]) exitWith {"Already running"};
    _originalGroup setVariable ["TOV_caching_running", True];
    
    //Don't work on vehicles
    _leaderGroup = leader _originalGroup;
    if (vehicle _leaderGroup != _leaderGroup) exitWith {"Group in vehicle"};
    
    _timeOut = 600; //in seconds

    _startTimeOut = _timeOut;

    while {_startTimeOut > 0} do {
        
        _eligibleUnits = (units _originalGroup) select {alive _x};


        if ((count _eligibleUnits) ==0) exitWith {True};

        sleep 30 ;
        _startTimeOut = _startTimeOut - 30;

        _closePlayers = allPlayers select {_x distance (leader _originalGroup) < _playersDistance};
        if (count _closePlayers > 0) then {
            _startTimeOut = _timeOut;
        };

    };

    //If everyone is dead stop
    _eligibleUnits = (units _originalGroup) select {alive _x};
    if ((count _eligibleUnits) ==0) exitWith {True};

    //Else, prepare the next respawn
    _groupSide = side _originalGroup;
    _groupPos = getPosATL (leader _originalGroup);
    
    _groupUnits = [];
    {_groupUnits pushBack (typeOf _x) } forEach _eligibleUnits;

    {deleteVehicle _x} forEach (units _originalGroup) ;
    [_originalGroup] remoteExec ["deleteGroup", groupOwner _originalGroup];

    //Waiting until players comes back
    waitUntil {sleep (5 + random 15); (count (allplayers select {(_x distance _groupPos) < _playersDistance})) > 0} ;

    //Spawning
    _newGroup = createGroup [_groupSide, True] ;
    { _newGroup createUnit [_x, _groupPos, [], 0, "NONE"]; } forEach _groupUnits;

    if (_garrison) then {
        //Garrison or patrol
        [_newGroup, _newGroup, 600, 7, "MOVE", "SAFE", "YELLOW", "LIMITED", "COLUMN", "", [3, 6, 9]] call CBA_fnc_taskPatrol;

        if (random 1 < 0.7) then {
            [getpos (leader _newGroup), nil, units _newGroup, 1500, 0, selectRandom [True,False], true] call ace_ai_fnc_garrison;                      
            {_x setUnitPos "UP"; if (random 1 >0.8) then {_x disableAI "PATH"};} forEach units _newGroup;
        };

    } else {
        //Hunt
                [_newGroup, _newGroup, 2500, 7, "MOVE", "SAFE", "YELLOW", "LIMITED", "COLUMN", "", [3, 6, 9]] call CBA_fnc_taskPatrol;
		_hunt = {params ["_defenders"]; [_defenders, 5000, 15, [], [], true, true, 2] spawn lambs_wp_fnc_taskHunt;};
		_creep = {params ["_defenders"]; [_defenders, 5000, 15, [], [], true] spawn lambs_wp_fnc_taskCreep;};
		_rush = {params ["_defenders"]; [_defenders, 5000, 15, [], [], true] spawn lambs_wp_fnc_taskRush;};
	
		_to_run = selectRandom [_hunt, _creep, _rush];
		[_newGroup] spawn _to_run;
    };

    //Enable dynamic sim and renable respawn
    _newGroup enableDynamicSimulation True;
    [_newGroup, _garrison, _playersDistance] spawn TOV_caching;
};




//Harcoded reset of south asia replacement
//TOV_REPLACEMENT_POSITIONS=[screenToWorld [0.5,0.5]];
//TOV_REPLACEMENT_RADIUS=2000;

_new_pos_array=[];
{
    _requestedTypes=["BUILDING", "HOUSE", "CHURCH", "CHAPEL", "VIEW-TOWER", "LIGHTHOUSE", "HOSPITAL", "HIDE", "STACK", "RUIN", "TOURISM", "WATERTOWER"];
    _center=_x;
    _radius=TOV_REPLACEMENT_RADIUS;

    //skip if player too close
    if ((count (allplayers select {(_x distance _center) < 5000})) > 0) then { _new_pos_array pushBack _center; continue} ;


    _hiddenBuildings = nearestTerrainObjects [_center, _requestedTypes, _radius, false]; 
    _replacedBuildings = nearestObjects [_center, [], _radius];

    _replacedBuildings = _replacedBuildings select {(["EP1", (typeOf _x)] call BIS_fnc_inString)};
    _hiddenBuildings = _hiddenBuildings select {isObjectHidden _x};

    //Deleting the old, and getting the new ones
    {deleteVehicle _x} forEach _replacedBuildings;
    {[_x, false] remoteExec ["hideObjectGlobal", 2]} forEach _hiddenBuildings;

} forEach TOV_REPLACEMENT_POSITIONS;


TOV_REPLACEMENT_POSITIONS = _new_pos_array;
publicVariable "TOV_REPLACEMENT_POSITIONS";
