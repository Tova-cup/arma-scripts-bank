class CfgPatches
{
	class tov_aiImprover
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {"cba_xeh_a3"};
	};
};

class Extended_PreInit_EventHandlers
{
	class tov_aiImprover
	{
		clientInit = "tov_aiImprover_enabled_Var = [] execVM 'tov_aiImprover\AI-master-script.sqf'";
	};
};
