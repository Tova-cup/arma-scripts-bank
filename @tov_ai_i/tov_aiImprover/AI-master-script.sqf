//CBA Settings/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Improved AI
[
    "TOV_AI_improvedAI", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "CHECKBOX", // setting type
    "Enable smarter friendly AI", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Tova's AI improver optional commands", // Pretty name of the category where the setting can be found. Can be stringtable entry.
    True, // data for this setting: [min, max, default, number of shown trailing decimals]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {} // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

//ACE Healing
[
    "TOV_AI_allowAceHealing", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "CHECKBOX", // setting type
    "ACE heal command", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Tova's AI improver optional commands", // Pretty name of the category where the setting can be found. Can be stringtable entry.
    False, // data for this setting: [min, max, default, number of shown trailing decimals]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {} // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

//Storing/retrieving units
[
    "TOV_AI_allowStoringUnits", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "CHECKBOX", // setting type
    "Allow storing units", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Tova's AI improver optional commands", // Pretty name of the category where the setting can be found. Can be stringtable entry.
    False, // data for this setting: [min, max, default, number of shown trailing decimals]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {} // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

//Teleporting units
[
    "TOV_AI_allowTeleports", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "CHECKBOX", // setting type
    "Allow teleporting units in nearby buildings", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Tova's AI improver optional commands", // Pretty name of the category where the setting can be found. Can be stringtable entry.
    False, // data for this setting: [min, max, default, number of shown trailing decimals]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {} // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

//Teleporting units range
[
    "TOV_AI_allowTeleports_range", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "SLIDER", // setting type
    "Max distance to allow teleporting units in nearby buildings", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Tova's AI improver optional commands", // Pretty name of the category where the setting can be found. Can be stringtable entry.
    [0, 300, 30, 0], // data for this setting: [min, max, default, number of shown trailing decimals]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {} // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

//Teleporting type
[
    "TOV_AI_Teleports_type", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "LIST", // setting type
    "Teleport to building pos or exact position ?", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Tova's AI improver optional commands", // Pretty name of the category where the setting can be found. Can be stringtable entry.
    [["pos", "exact"], ["Building Pos","Exact Pos"], 0], // data for this setting: [_values, _valueTitles, _defaultIndex]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {} // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

//Teleporting units on breach
[
    "TOV_AI_teleportsBreach", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "CHECKBOX", // setting type
    "Allow units to teleport to bulding entrance if path cannot be calculated by the engine", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Tova's AI improver optional commands", // Pretty name of the category where the setting can be found. Can be stringtable entry.
    False, // data for this setting: [min, max, default, number of shown trailing decimals]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {} // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

//Teleporting units on breach range
[
    "TOV_AI_teleportsBreach_range", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "SLIDER", // setting type
    "Max distance to allow units to teleport to bulding entrance if path cannot be calculated by the engine", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Tova's AI improver optional commands", // Pretty name of the category where the setting can be found. Can be stringtable entry.
    [0, 300, 30, 0], // data for this setting: [min, max, default, number of shown trailing decimals]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {} // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

//Damage reduction value
[
    "TOV_AI_damage_multiplier", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "SLIDER", // setting type
    "Damage multiplier to units", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Tova's AI improver optional commands", // Pretty name of the category where the setting can be found. Can be stringtable entry.
    [0.01, 1, 0.1, 2], // data for this setting: [min, max, default, number of shown trailing decimals]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {} // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

//Max damage per bullet
[
    "TOV_AI_damage_limit", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "SLIDER", // setting type
    "Maximum damage per bullet", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Tova's AI improver optional commands", // Pretty name of the category where the setting can be found. Can be stringtable entry.
    [0.01, 1, 1, 2], // data for this setting: [min, max, default, number of shown trailing decimals]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {} // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

//Liberation commands
[
    "TOV_AI_allowLiberationOrders", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "CHECKBOX", // setting type
    "Liberation specific orders", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Tova's AI improver optional commands", // Pretty name of the category where the setting can be found. Can be stringtable entry.
    False, // data for this setting: [min, max, default, number of shown trailing decimals]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {} // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;


//
//DEFINING THE ADVANCED AI FUNCTIONS
//

damageReducer=compile preprocessFileLineNumbers "tov_aiImprover\AI-damage-reduction.sqf";


TOV_isStuck={
	params ["_unitToMonitor",["_minSpeed",2],["_timeForStuck",10]];
	_maxWindowSize=10;
	_sleepTime=_timeForStuck/_maxWindowSize;
	
	_speedVector=[];

	sleep 3;
	while {(formleader _unitToMonitor == _unitToMonitor) && (alive _unitToMonitor)} do {
		sleep _sleepTime;
		_speedVector pushBack (speed _unitToMonitor);
		if (count _speedVector>_maxWindowSize) then {_speedVector deleteAt 0;};
		if (count _speedVector<_maxWindowSize) then {continue;};

		_avgSpeed=0;
		for "_i" from 0 to (count _speedVector)-1 do {_avgSpeed=_avgSpeed + (_speedVector select _i)};
		_avgSpeed=_avgSpeed/(count _speedVector);
		
		if (_avgSpeed < _minSpeed) then 
		{_unitToMonitor setVariable ["TOV_unitStuck",True];} else 
		{_unitToMonitor setVariable ["TOV_unitStuck",False];};

	};
	_unitToMonitor setVariable ["TOV_unitStuck",False];
};


TOV_a_breachIn={ 
    //params ["_group", "_targetBuilding"]; 
	params["_target","_pos","_is3D","_unitsToBreach"];
	//hint format ["target %1", _target];
	
	_building=[];
	_unitsToBreach= _unitsToBreach select {vehicle _x==_x}; 
	if ((isObjectHidden _target) || (isnil "_target") || !(_is3D)) then {
		_building = nearestObjects [_pos, ["House", "Building"], 50] ;
	} else {
		_building = nearestObjects [_target, ["House", "Building"], 50] ;
	};
	if (count _building == 0) exitwith {};
	_building =_building select { !(isObjectHidden _x)};
	_building=_building select 0;
	
	_firstLoop=True;
	
    // Search while there are still available positions 
    private _positions = _building buildingPos -1;
	/*
	_positions=_positions apply {AGLToASL _x};
	//_positions=[_positions, [], { ((_unitsToBreach#0) distance2D (ASLToAGL _x)) + 1000*(_x#2) }, "ASCEND"] call BIS_fnc_sortBy;
	_closestEntry=[_positions, [], { ((_unitsToBreach#0) distance2D (ASLToAGL _x)) + 1000*(_x#2) }, "ASCEND"] call BIS_fnc_sortBy;
	_positions= [_closestEntry#0] + _positions;
	_positions=_positions apply {ASLToAGL _x};
	*/
	
	{[_x] spawn TOV_isStuck } forEach _unitsToBreach;
	
    while {sleep 0.1;!(_positions isEqualTo [])} do { 
        // Update units in case of death 
        private _units = (_unitsToBreach select { alive _x}) - [player]; 
 
        // Abort search if the group has no units left or if player asked units to regroup
        if (	
				(_firstLoop==False) &&
				(
					(_units isEqualTo []) ||
					(({formleader _x == _x} count _units)==0)
				)
			) exitWith {}; 
		
        // Send all available units to the next available position 
        {
			//EXPERIMENTAL
			//remove positions units passed by while not being destination
			_currentUnit=_x;
			_positions=_positions select {
				//((_currentUnit distance _x)>1.5) ||
				_posToCheck=AGLToASL _x;
				(((getPos _currentUnit)#2 - _x#2)>1) || //positions on different floors
				!(!(lineIntersects [eyePos _currentUnit, [_posToCheck#0,_posToCheck#1,1+_posToCheck#2], _currentUnit]) && (_currentUnit distance _x)<10) // NOT [(positions you can see) and (closer than 3 meters)]
				};
			//hintSilent str (count _positions);
			
			
			//END OF EXPERIMENTAL
            if (_positions isEqualTo []) exitWith {};
			if (_firstLoop) then {
				//Send everyone to the entrance
				{_x doMove _positions#0;} forEach _units;
				
				if (TOV_AI_teleportsBreach) then {
					//hint "test";
					_tpPosition=_positions#0;
					[_tpPosition,_x] spawn {
						params["_tpPosition","_myUnit"];
						waitUntil {sleep 0.1; 
							(
								((unitReady _myUnit) && (formleader _myUnit == _myUnit))
								|| (_myUnit getVariable ["TOV_unitStuck",False])
								|| (_myUnit distance _tpPosition<2)
							)
						};
						if (
							(_myUnit getVariable ["TOV_unitStuck",False]) && (_myUnit distance _tpPosition>2) && (_myUnit distance _tpPosition<TOV_AI_teleportsBreach_range)
							) then {_myUnit setPosATL _tpPosition;}
					};
				};
				_positions deleteAt 0;
			};
			
			
            if (
				(
					(unitReady _x) &&
					(formleader _x == _x)
				)
				|| (_x getVariable "TOV_unitStuck")
			) then { 
                private _pos = _positions deleteAt 0; 
                _x doMove _pos; 
                sleep 0.2; 
            }; 
        } forEach _units;
		
		_firstLoop=False;
    }; 
};


TOV_a_healNearestPlayer={
	params ["_medic"];
	_closestPlayers=_medic nearEntities ["Man", 20];
	_closestPlayers=_closestPlayers select {isPlayer _x;};

	_healQueue = _medic getVariable ["ace_medical_ai_healQueue", []]; 
	_healQueue =_healQueue + _closestPlayers;
	_medic setVariable ["ace_medical_ai_healQueue", _healQueue];
};

TOV_a_unstickUnit={
	params ["_unit"];
	if ( isPlayer _unit || { !local _unit } || (vehicle _unit != _unit) ) exitWith {}; 
	_newPos=[_unit, 0, 15, 1] call BIS_fnc_findSafePos;
	if (count _newPos == 2) then {
		_unit setPos _newPos;
	};
	//_unit setVehiclePosition [_pos vectorAdd [0,0,_add], [], 0, "NONE"];
	_unit doWatch objNull;
	//_unit selectWeapon (primaryWeapon _unit);
	_unit switchMove "";
	_unit setVelocity [0,0,0];
	_unit forceSpeed -1;
};

TOV_a_TP_buildingPos={
	params["_target","_pos","_is3D","_unitsToTeleport"];
	//hint format ["target %1", _target];
	
	if (!_is3D || !(_target isKindOf "Building")) exitwith {False};
	
	_unitsToTeleport = _unitsToTeleport select {(_x distance2D _target <TOV_AI_allowTeleports_range) && (vehicle _x==_x)}; 
	
	{
		_x setPosATL _pos;
		dostop _x;
	} forEach _unitsToTeleport;
};

TOV_a_TP_exact={
	//teleport to exact position
	params["_target","_pos","_is3D","_unitsToTeleport"];
	//hint format ["target %1", _target];
	
	//if (!_is3D || !(_target isKindOf "Building")) exitwith {False};
	
	private _distance = viewDistance;
        private _origin = AGLToASL positionCameraToWorld [0, 0, 0];
        private _target = AGLToASL positionCameraToWorld [0, 0, _distance];

        private _default = _origin vectorAdd (_origin vectorFromTo _target vectorMultiply _distance);
        _target = lineIntersectsSurfaces [_origin, _target, cameraOn] param [0, [_default]] select 0;
	
	_unitsToTeleport = _unitsToTeleport select {(_x distance2D _target <TOV_AI_allowTeleports_range) && (vehicle _x==_x)}; 
	
	{
		_x setPosASL _target; //NOTE THE ASL
		dostop _x;
	} forEach _unitsToTeleport;
};

TOV_a_TP={
	params["_target","_pos","_is3D","_unitsToTeleport"];
	
	if (TOV_AI_Teleports_type=="pos") then {
		[_target,_pos,_is3D,_unitsToTeleport] spawn TOV_a_TP_buildingPos;
	} else {
		[_target,_pos,_is3D,_unitsToTeleport] spawn TOV_a_TP_exact;
	};
	
};


TOV_a_limitSpeed={
	params ["_selectedSpeed"];
	{
		if (vehicle _x isKindOf 'land') then {
			(vehicle _x) limitSpeed _selectedSpeed;
		}
	} forEach (groupSelectedUnits player);
};
TOV_LimitSpeedSubmenu=
[
	["Limit Speed",true],
	["30 km/h", [2], "", -5, [["expression", "30 call TOV_a_limitSpeed"]], "IsLeader", "NotEmpty"],
	["50 km/h", [3], "", -5, [["expression", "50 call TOV_a_limitSpeed"]], "IsLeader", "NotEmpty"],
	["70 km/h", [4], "", -5, [["expression", "70 call TOV_a_limitSpeed"]], "IsLeader", "NotEmpty"],
	["90 km/h", [5], "", -5, [["expression", "90 call TOV_a_limitSpeed"]], "IsLeader", "NotEmpty"],
	["120 km/h", [6], "", -5, [["expression", "120 call TOV_a_limitSpeed"]], "IsLeader", "NotEmpty"],
	["No limit", [7], "", -5, [["expression", "-1 call TOV_a_limitSpeed"]], "IsLeader", "NotEmpty"]
];


TOV_a_noProne= {
	params ["_unit"];
	//if !(isServer) exitWith {false};
	
	_unit setVariable ["TOV_noProne",true];
	while {alive _unit && _unit getVariable ["TOV_noProne",true]} do {
		if (behaviour _unit in ["COMBAT","STEALTH","AWARE"]) then {
			waitUntil {sleep 1; speed _unit < 1};
			_unit setUnitpos "MIDDLE";
			waitUntil {sleep 1; speed _unit > 2};
			_unit setUnitpos "UP";
		} else {
			_unit setUnitpos "AUTO";
			sleep 2;
		};
	};
	_unit setUnitpos "AUTO";
	_unit setVariable ["TOV_noProne",false];
};
TOV_NoProneSubmenu=
[
	["No Prone Mode",true],
	["Enable", [2], "", -5, [["expression", "{[_x] spawn TOV_a_noProne;} forEach (groupSelectedUnits player);"]], "IsLeader", "NotEmpty"],
	["Disable", [3], "", -5, [["expression", "{_x setVariable ['TOV_noProne',false];} forEach (groupSelectedUnits player);"]], "IsLeader", "NotEmpty"]
];

	
TOV_a_limitAltitude={
	params ["_selectedAltitude"];
	{
		if (vehicle _x isKindOf 'air') then {
			(vehicle _x) flyInHeight _selectedAltitude;
		}
	} forEach (groupSelectedUnits player);
};
TOV_LimitAltitudeSubmenu=
[
	["Limit Altitude",true],
	["20 m", [2], "", -5, [["expression", "20 call TOV_a_limitAltitude"]], "IsLeader", "NotEmpty"],
	["50 m", [3], "", -5, [["expression", "50 call TOV_a_limitAltitude"]], "IsLeader", "NotEmpty"],
	["70 m", [4], "", -5, [["expression", "70 call TOV_a_limitAltitude"]], "IsLeader", "NotEmpty"],
	["100 m", [5], "", -5, [["expression", "100 call TOV_a_limitAltitude"]], "IsLeader", "NotEmpty"],
	["150 m", [6], "", -5, [["expression", "150 call TOV_a_limitAltitude"]], "IsLeader", "NotEmpty"],
	["200 m", [7], "", -5, [["expression", "200 call TOV_a_limitAltitude"]], "IsLeader", "NotEmpty"],
	["500 m", [8], "", -5, [["expression", "500 call TOV_a_limitAltitude"]], "IsLeader", "NotEmpty"],
	["800 m", [9], "", -5, [["expression", "800 call TOV_a_limitAltitude"]], "IsLeader", "NotEmpty"],
	["1000 m", [10], "", -5, [["expression", "1000 call TOV_a_limitAltitude"]], "IsLeader", "NotEmpty"]
];


TOV_SpeedModeSubmenu=
[
	["Set speed mode",true],
	["Normal", [2], "", -5, [["expression", "group player setSpeedMode 'NORMAL'"]], "IsLeader", "NotEmpty"],
	["Limited", [3], "", -5, [["expression", "group player setSpeedMode 'LIMITED'"]], "IsLeader", "NotEmpty"],
	["Full", [4], "", -5, [["expression", "group player setSpeedMode 'FULL'"]], "IsLeader", "NotEmpty"]
];

TOV_a_storeUnits= {
	params ["_units"];
	
	//Create group to store units into
	_group = player getVariable ["TOV_storedGroup",createGroup [side player,False]];
	if (isNull _group) then {_group=createGroup [side player,False]};
	
	_reference= formLeader (_units select 0);
	if (isPlayer _reference) then { _reference= vehicle (_units select 0)};
	expectedDestination _reference  params ["_Ldest","_Lstatus","__"] ;
	
	{
		//finding unit ID pos in the Group
		_str = str _x;
		_unitIdPos=parseNumber (_str select [(_str find ":") + 1]);
		_x setVariable ["TOV_unit_team",assignedTeam _x];
		_unitBehaviour=behaviour _x;
		_unitCCombatMode=unitCombatMode _x;
		
		expectedDestination _x params ["_dest","_status","__"] ;

		//joining the storing group
		_x joinAsSilent [_group, _unitIdPos];
		_x setBehaviour _unitBehaviour;
		_x setUnitCombatMode _unitCCombatMode;
		if (
			(_status in ["LEADER PLANNED","VEHICLE PLANNED"]) ||
			((_status in ["DoNotPlanFormation","FORMATION PLANNED"]) && !(isPlayer leader _x) && !(_status in ["DoNotPlan"]) && (_Lstatus in ["LEADER PLANNED","VEHICLE PLANNED"]))
			)
			then {_x doMove _Ldest} else {doStop _x};
	} forEach (_units);
	
	//Storing the group in a player variable
	player setVariable ["TOV_storedGroup",_group];
};
TOV_a_retrieveUnits= {
	//Retrieving the storing group
	_group = player getVariable ["TOV_storedGroup",createGroup [side player,False]];
	if (isNull _group) then {_group=createGroup [side player,False]};
	
	expectedDestination (leader _group) params ["_Ldest","_Lstatus","__"] ;
	
	{
		//finding unit ID pos in the Group
		_str = str _x;
		_unitIdPos=parseNumber (_str select [(_str find ":") + 1]);
		_unitBehaviour=behaviour _x;
		_unitCCombatMode=unitCombatMode _x;
		expectedDestination _x params ["_dest","_status","__"] ;
		
		//joining the storing group
		_x joinAsSilent [group player, _unitIdPos];
		_x assignTeam (_x getVariable ["TOV_unit_team","WHITE"]);
		_x setBehaviour _unitBehaviour;
		_x setUnitCombatMode _unitCCombatMode;

		if (
			(_status in ["LEADER PLANNED","VEHICLE PLANNED"]) ||
			((_status in ["DoNotPlanFormation","FORMATION PLANNED"]) && !(isPlayer leader _x) && (_status in ["DoNotPlan"]) && (_Lstatus in ["LEADER PLANNED","VEHICLE PLANNED"]))
			) 
			then {_x doMove _Ldest} else {doStop _x};
	} forEach (units _group);
	
	//Storing the group in a player variable
	player setVariable ["TOV_storedGroup",_group];
};
TOV_storeUnitsSubmenu=
[
	["Store/retrieve units",true],
	["Store selected", [2], "", -5, [["expression", "[groupSelectedUnits player] call TOV_a_storeUnits;"]], "IsLeader", "NotEmpty"],
	["Retrieve stored units", [3], "", -5, [["expression", "call TOV_a_retrieveUnits;"]], "1", "1"]
];

//Liberation orders
TOV_moveSingleLibCrate={
	params["_unitToCarry","_crateToBeCarried","_dropOffPoint"];
	
	_unitToCarry setVariable ["TOV_busyWithCrate",True];
	
	_resetState={
		_finalPos=[_crateToBeCarried, 0, 10] call BIS_fnc_findSafePos;
		detach _crateToBeCarried;
		if ((count _finalPos) == 2) then {_crateToBeCarried setPos _finalPos};
		_unitToCarry forcewalk false;
		_unitToCarry setVariable ["TOV_busyWithCrate",False];
	};
	
	_nextStepCondition = {
		(!alive _unitToCarry) ||
		(unitReady _unitToCarry) ||
		(formleader _unitToCarry != _unitToCarry) ||
		(stopped _unitToCarry) ||
		(behaviour _unitToCarry == "COMBAT")
	};
		
	_unitToCarry doMove getPos _crateToBeCarried;
	waitUntil {sleep 0.1; call _nextStepCondition};
	if (_unitToCarry distance _crateToBeCarried > 10 ) exitWith {call _resetState}; 
	
	_crateToBeCarried attachTo [_unitToCarry, [0, 1.2, 1]];
	_unitToCarry forcewalk true;
	_unitToCarry doMove _dropOffPoint;
	waitUntil {sleep 0.1; call _nextStepCondition};
	
	call _resetState;
};
TOV_moveLibCrates={
	params["_dropOffPoint","_unitsToCarryStuff"];
	_cratesTypes=[KP_liberation_supply_crate,KP_liberation_ammo_crate,KP_liberation_fuel_crate];  

	
	_unitsToCarryStuff= (_unitsToCarryStuff select {vehicle _x==_x}) - [player];
	
	_boxesToMove=[];
	{
		_boxesFound= nearestObjects [_x, _cratesTypes, 100]; 
		_boxesFound= _boxesFound select {isNull attachedTo _x};
		_boxesFound= _boxesFound select {_x distance _dropOffPoint >10};
		_boxesToMove= _boxesToMove + _boxesFound;
	} forEach _unitsToCarryStuff;
	_boxesToMove= _boxesToMove arrayIntersect _boxesToMove;
	_boxesToMove;
	
	while {sleep 0.1;!(_boxesToMove isEqualTo [])} do { 
		// Update units in case of death 
		private _units = (_unitsToCarryStuff select { alive _x}) - [player]; 

		// Abort carry if the group has no units left or if player asked units to regroup
		if (	
				(_units isEqualTo []) ||
				(({formleader _x == _x} count _units)==0) ||
				(({!(stopped _x) } count _units)==0) 
			) exitWith {};

		// Assign a crate to each dude
		{ 
			if !(_boxesToMove isEqualTo []) then {
				private _currentCrate = _boxesToMove deleteAt 0; 
				[_x,_currentCrate,_dropOffPoint] spawn Tov_moveSingleLibCrate;
				sleep 0.2;
			};
		} forEach (_units select {!(_x getVariable ["TOV_busyWithCrate",false])});
		
	}; 
};

TOV_land_heli={
	params["_unitsToLand"];
	{
		vehicle _x land "GET IN";
	} forEach _unitsToLand;
};


TOV_LiberationSubmenu=
[
	["Liberation orders",true],
	["Move nearby crates", [2], "", -5, [["expression", "[_pos, groupSelectedUnits player] spawn Tov_moveLibCrates"]], "IsLeader", "NotEmpty","\a3\Ui_f\data\IGUI\Cfg\Cursors\tactical_ca.paa"],
	
	["Land (engine on)", [3], "", -5, [["expression", "[groupSelectedUnits player] spawn TOV_land_heli"]], "IsLeader", "NotEmpty","\a3\Ui_f\data\IGUI\Cfg\Cursors\tactical_ca.paa"]
];

/*
//Option to land
if (({(vehicle _x != _x) && (vehicle _x isKindOf "air")} count (groupSelectedUnits player)>0)) then {
	TOV_LiberationSubmenu pushback 
	["Land (engine on)", [3], "", -5, [["expression", "[groupSelectedUnits player] spawn TOV_land_heli"]], "IsLeader", "NotEmpty","\a3\Ui_f\data\IGUI\Cfg\Cursors\tactical_ca.paa"];
};
*/

//
//
//  BUILDING THE MENU PROPER
//


TOV_CommandMenu = 
[
	// First array: "User menu" This will be displayed under the menu, bool value: has Input Focus or not.
	// Note that as to version Arma2 1.05, if the bool value set to false, Custom Icons will not be displayed.
	["Advanced Commands",false]
	// Syntax and semantics for following array elements:
	// ["Title_in_menu", [assigned_key], "Submenu_name", CMD, [["expression",script-string]], "isVisible", "isActive" <, optional icon path> ]
	// Title_in_menu: string that will be displayed for the player
	// Assigned_key: 0 - no key, 1 - escape key, 2 - key-1, 3 - key-2, ... , 10 - key-9, 11 - key-0, 12 and up... the whole keyboard
	// Submenu_name: User menu name string (eg "#USER:MY_SUBMENU_NAME" ), "" for script to execute.
	// CMD: (for main menu:) CMD_SEPARATOR -1; CMD_NOTHING -2; CMD_HIDE_MENU -3; CMD_BACK -4; (for custom menu:) CMD_EXECUTE -5
	// script-string: command to be executed on activation. (no arguments passed)
	// isVisible - Boolean 1 or 0 for yes or no, - or optional argument string, eg: "CursorOnGround"
	// isActive - Boolean 1 or 0 for yes or no - if item is not active, it appears gray.
	// optional icon path: The path to the texture of the cursor, that should be used on this menuitem.
];

//showCommandingMenu "#USER:MY_MENU_inCommunication";

//Building the command menu
TOV_calculateMenu={
	TOV_CommandMenu=
	[
		["Advanced Commands",true]
	];
	
		
	//Breach building
	if ((count (groupSelectedUnits player) >0)) then {
		TOV_CommandMenu pushback 
		["Breach building", [4], "", -5, [["expression", '[_target,_pos,_is3D,groupSelectedUnits player] spawn TOV_a_breachIn']], "IsLeader", "NotEmpty","\a3\Ui_f\data\IGUI\Cfg\Cursors\iconCursorSupport_ca.paa"];
	};
	
	//Limit a vehicle speed
	if (({(vehicle _x != _x) && (vehicle _x isKindOf "land")} count (groupSelectedUnits player)>0)) then {
		TOV_CommandMenu pushback 
		["Limit Speed", [3], "#USER:TOV_LimitSpeedSubmenu", -5, [["expression", ""]], "1", "1"];
	};
	
	//Limit a vehicle altitude
	if (({(vehicle _x != _x) && (vehicle _x isKindOf "air")} count (groupSelectedUnits player)>0)) then {
		TOV_CommandMenu pushback 
		["Limit Altitude", [3], "#USER:TOV_LimitAltitudeSubmenu", -5, [["expression", ""]], "1", "1"];
	};
	
	//Make player vehicle commander
	if (((vehicle player) != player) && ({ group _x == group player  } count (crew vehicle player)>1)) then {
		TOV_CommandMenu pushback 
		["Direct command vehicle on/off", [4], "", -5, [["expression", "_veh=vehicle player; if ((effectiveCommander _veh) == player) then {_veh setEffectiveCommander driver _veh} else {_veh setEffectiveCommander player}"]], "1", "1"];
	};
	
	//Set group speed mode
	if ((count (groupSelectedUnits player) >0)) then {
		TOV_CommandMenu pushback 
		["Set speed mode", [5], "#USER:TOV_SpeedModeSubmenu", -5, [["expression", ""]], "1", "1"];
	};
	
	//Prone mode
	if ((count (groupSelectedUnits player) >0)) then {
		TOV_CommandMenu pushback 
		["No Prone Mode", [6], "#USER:TOV_NoProneSubmenu", -5, [["expression", ""]], "1", "1"];
	};
	
	//Store retrieve units
	if (TOV_AI_allowStoringUnits) then {
		TOV_CommandMenu pushback 
		["Store/retrieve units", [7], "#USER:TOV_storeUnitsSubmenu", -5, [["expression", ""]], "1", "1"];
	};
	
	//Allow to heal because unit is medic
	if (TOV_AI_allowAceHealing && ({_x getUnitTrait "Medic";} count (groupSelectedUnits player)>0) && (count (groupSelectedUnits player) ==1)) then {
		TOV_CommandMenu pushback 
		["Heal nearest players", [9], "", -5, [["expression", '(groupSelectedUnits player) spawn TOV_a_healNearestPlayer']], "IsLeader", "NotEmpty"];
	};

	//Unstick units
	if ((count (groupSelectedUnits player) >0)) then {
		TOV_CommandMenu pushback 
		["Unstick unit", [10], "", -5, [["expression", '{
			_x call tov_a_unstickUnit;
		} forEach ((groupSelectedUnits player) select {!(isPlayer _x)})']], "IsLeader", "NotEmpty"];
	};
	
	if (TOV_AI_allowTeleports && (!visibleMap) && ({vehicle _x == _x } count (groupSelectedUnits player)>0)) then {
	TOV_CommandMenu pushback 
		["Teleport to building position", [11], "", -5, [["expression", '[_target,_pos,_is3D,groupSelectedUnits player] spawn TOV_a_TP']], "IsLeader", "NotEmpty","\a3\Ui_f\data\IGUI\Cfg\Cursors\iconCursorSupport_ca.paa"];
	};
	
	//liberation stuff
	if (
			TOV_AI_allowLiberationOrders &&
			(count (groupSelectedUnits player) >0) 
		)
	then {
		TOV_CommandMenu pushback ["Liberation submenu", [11], "#USER:TOV_LiberationSubmenu", -5, [["expression", ""]], "1", "1"];
	};
};


///////////////Hotkey for custom menu
/*
///Vanilla
TOV_customMenuKeys=actionkeys "User5";
0 = [] spawn {
  waitUntil {!isNull findDisplay 46};
  if (isnil "TOV_customMenu") then {  
  TOV_customMenu = (findDisplay 46) displayAddEventHandler ["KeyDown",  
    "
      private _handled = false;  
      if (
		((_this select 1) in TOV_customMenuKeys) && 
		(leader player == player) && 
		({!(isPlayer _x)} count (units player)>0)
		)
		then {
		call TOV_calculateMenu;
		showCommandingMenu '#USER:TOV_CommandMenu';
        _handled = true  
      };  
      _handled  
    "];
  };
};
*/

//with CBA
TOV_OpenMenu={
    if (
		(leader player == player) 
		)
		then {
		call TOV_calculateMenu;
		showCommandingMenu '#USER:TOV_CommandMenu';
      };  
};
["Tova's AI Improver","TOV_openCommandMenu", "Open the advanced commanding menu", {call TOV_OpenMenu}, "", [12, [False, False, False]]] call CBA_fnc_addKeybind;


// LOOP TO REPEAT AI FIXES//


[] spawn {
	sleep 30;
	while {True} do {
		if (!TOV_AI_improvedAI) then {sleep 60;continue};
		{
			if (_x getVariable ["tov_aiImprover_notInitialized",true]) then {
				_x setSkill 1; 
				_x disableAI "AIMINGERROR";
				[_x] call damageReducer;
				_x setVariable ["lambs_danger_disableAI",true];
				_x setVariable ["tov_aiImprover_notInitialized",false];
			}
		} forEach (units player);
		sleep 60;
	};
};
