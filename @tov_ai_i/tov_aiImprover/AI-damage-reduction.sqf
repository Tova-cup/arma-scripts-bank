params[ "_unit" ];

// Damage reduction from https://forums.bistudio.com/forums/topic/198136-reducing-player-damage/

// Exit if we are a player and not local
// Otherwise add EH for AI every where just in case their locality
// changes due to moving into a players group
// the EH will only fire where the AI is local﻿
if ( isPlayer _unit || { !local _unit } ) exitWith {};


_unit removeAllEventHandlers 'HandleDamage';


// Only damage from last applied handleDamage EH is taken into consideration by the engine
// Apply a new EH so as we can override the damage applied
_unit addEventHandler [ "HandleDamage", {
	params ["_unit", "_hitSelection", "_damage","_source","_projectile","_hitPartIndex", "_instigator", "_hitPoint"];

	// Damage multipliers.  The damage of each projectile will be multiplied by this number.
	private _damageMultiplierHead = TOV_AI_damage_multiplier;
	private _damageMultiplierBody = TOV_AI_damage_multiplier;
	private _damageMultiplierLimbs = TOV_AI_damage_multiplier;
	private _damageMultiplierOverall = TOV_AI_damage_multiplier;

	// Damage limits.  Each projectile will be limited to a max of this much damage.
	private _limitHead = TOV_AI_damage_limit * 5;
	private _limitBody = TOV_AI_damage_limit;
	private _limitLimbs = TOV_AI_damage_limit;
	private _limitOverall = TOV_AI_damage_limit;

	private _oldDamage = 0;
	if (_hitSelection isEqualTo "") then {_oldDamage = damage _unit} else {_oldDamage = _unit getHit _hitSelection};
	private _newDamage = _damage - _oldDamage max 0;
	private _incomingDamage = _newDamage;
	private _playerHealth = damage _unit;

	// Infantry selections
	// Keep in mind that if revive is enabled then incapacitation may occur at around 0.7 damage.
	// "": The overall damage that determines the damage value of the unit. Unit dies at damage equal to or above 1
	// "face_hub": Unit dies at damage equal to or above 1
	// "neck": Unit dies at damage equal to or above 1
	// "head": Unit dies at damage equal to or above 1
	// "pelvis": Unit dies at damage equal to or above 1
	// "spine1": Unit dies at damage equal to or above 1
	// "spine2": Unit dies at damage equal to or above 1
	// "spine3": Unit dies at damage equal to or above 1
	// "body": Unit dies at damage equal to or above 1
	// "arms": Unit doesn't die with damage to this part
	// "hands": Unit doesn't die with damage to this part
	// "legs": Unit doesn't die with damage to this part

	// Do any other damage calculations here
	// _damage is the previous damage plus any new damage and will be applied
	// as the total damage the unit has for this selection once this EH returns

	// Only modify damage if it is a known projectile (leave falling damage etc alone)
	if (_newDamage > 0 && !(_projectile isEqualTo "")) then {
		// Reduce damage by damage multiplier
		private _damageMultiplier = _damageMultiplierBody;
		private _upperLimit = _limitBody;
		switch (_hitSelection) do {
			case "face_hub";
			case "head": {
				_damageMultiplier = _damageMultiplierHead;
				_upperLimit = _limitHead;
			};
			case "arms";
			case "hands";
			case "legs": {
				_damageMultiplier = _damageMultiplierLimbs;
				_upperLimit = _limitLimbs;
			};
			case "": {
				_damageMultiplier = _damageMultiplierOverall;
				_upperLimit = _limitOverall;
			};
			default { 
				_damageMultiplier = _damageMultiplierBody;
				_upperLimit = _limitBody;
			};
		};
		_newDamage = _newDamage * _damageMultiplier;

		// Place an upper limit on projectile damage done at once
		if (_newDamage > _upperLimit) then {
			_newDamage = _upperLimit;
		};

		_damage = _oldDamage + _newDamage;
	};
	
	
	//Disable fall damage
	if (_newDamage > 0 && (_projectile isEqualTo "")) then {
		_damage = _oldDamage;
	};
	
	_damage
	
}];
