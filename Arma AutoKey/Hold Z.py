# Enter script code
try:
    toggle = store.get_global_value("toggle")
except TypeError:
    toggle = False #default setting

if toggle:
    keyboard.release_key("z")
    store.set_global_value("toggle", False)
else:
    store.set_global_value("toggle", True)
    keyboard.press_key("z")