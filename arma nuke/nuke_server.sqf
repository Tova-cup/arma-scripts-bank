nuke_smoke= compile preprocessFileLineNumbers "\z_sideMissions\fx_smoke.txt";
nuke_postprocess= compile preprocessFileLineNumbers "\z_sideMissions\fx_postprocess.txt";
nuke_shockwave= compile preprocessFileLineNumbers "\z_sideMissions\fx_shockwave.txt";
nuke_light= compile preprocessFileLineNumbers "\z_sideMissions\fx_light.txt";

publicvariable "nuke_smoke";
publicvariable "nuke_postprocess";
publicvariable "nuke_shockwave";
publicvariable "nuke_light";

nuke = {
	params["_pos","_yield","_radius"];

	_pos =[_pos select 0, _pos select 1, 2];

	if (isNil{_radius}) then {
		_radius = 20*_yield^0.4;
	};

	{
		[[_pos,_yield,_radius],_x] remoteExec ["spawn",0];
	}foreach [
		nuke_smoke,
		nuke_postprocess,
		nuke_shockwave,
		nuke_light
	];

	//[_pos,_yield,_radius] remoteExecCall ["rhs_fnc_ss21_nuke_fx_damage",2];

};
