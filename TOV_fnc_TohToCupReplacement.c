//Funtion to replace Take on Helicopters buildings by their CUP counterparts on Jove Chiere's South Asia map.
// Script need 2 parameters, the center and the radius for the replacement.
// Example : [player, 200] call TOV_fnc_TohToCupReplacement;
// 3den : [screenToWorld [0.5,0.5], 300] call TOV_fnc_TohToCupReplacement;  
// Replaces all ToH buildings that are within 200m around the player.

//The script intent is to replace buildings with a classname in the format "land_house_c_1_h" with their CUP counterpart "land_house_c_1_ep1"
TOV_fnc_TohToCupReplacement= {
	params ["_center","_radius"];
	
	_housesToReplace=nearestTerrainObjects [_center, [], _radius, false]; 
	//I would have wished to use _center nearestObjects ["House", _radius] instead, but none of the ToH buildings are considered a "House" by the engine.
	{ 
		//As some buildings of ToH return an empty string when I use typeOf (e.g land_house_c_1_h), I decided to use the name of the p3d model to retrieve the building classname
		if ((["house", (str _x)] call BIS_fnc_inString) || (["minaret", (str _x)] call BIS_fnc_inString) || (["villa", (str _x)] call BIS_fnc_inString)) then { 

			//Here I retrieve a string in the format "8add888100# 9: house_c_12_h.p3d" and turn it into the format "Land_house_c_12_h"
			_oldClass= (str _x) splitString "." select 0; 
		    _oldClass= _oldClass splitString ":" select 1; 
		    _oldClass= "Land_"+ (_oldClass splitString " " select 0);

		    _pos=getposATL _x; 
		    _dir=vectorDir _x; 
		    _up=vectorUp _x; 
		   
		    
			_tmp=_oldClass splitString "_"; 
			_tmp deleteAt (count _tmp)-1; 
			_newClass=""; 
			{_newClass=_newClass+_x+"_"} forEach _tmp; 
			_newClass=_newClass+"EP1"; 
		   

		   
			hideobjectGlobal _x; //I hide the ToH building as I cannot delete map buildings, also allow us to revert changes
			_x enableSimulationGlobal false;
			_newHouse=createVehicle [_newClass, _pos,[],0,"CAN_COLLIDE"];
			//_newHouse setPosASL _pos;
			_newHouse setVectorDirAndUp [_dir,_up]; 
		}; 
	} forEach _housesToReplace
};
